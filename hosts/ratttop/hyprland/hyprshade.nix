{
  config,
  pkgs,
  lib,
  ...
}: {
  xdg.configFile."hyprshade/config.toml".text = ''
    [[shades]]
    name = "vibrance"
    default = true

    [[shades]]
    name = "blue-light-filter"
    start_time = 04:00:00
    end_time = 13:00:00
  '';

  wayland.windowManager.hyprland.settings.exec-once = [
    "${lib.getExe pkgs.hyprshade} auto"
  ];

  systemd.user = {
    services.hyprshade = {
      Unit.Description = "Apply screen filter";

      Install.WantedBy = ["default.target"];

      Service = {
        Type = "oneshot";
        ExecStart = "${lib.getExe pkgs.hyprshade} auto";
        Environment = [
          "\"HYPRSHADE_SHADERS_DIR=${pkgs.hyprshade}/share/hyprshade/shaders/\""
          "\"PATH=${pkgs.hyprland}/bin:${pkgs.hyprshade}/bin\""
        ];
      };
    };

    timers.hyprshade = {
      Unit.Description = "Timer for screen filter";

      Install.WantedBy = ["timers.target"];

      Timer = {
        OnCalendar = "*:0/3";
      };
    };
  };
}
