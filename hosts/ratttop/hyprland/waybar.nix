{
  config,
  pkgs,
  lib,
  ...
}: {
  wayland.windowManager.hyprland.settings.exec-once = [
    (lib.getExe pkgs.waybar)
  ];

  programs.waybar = {
    enable = true;
    # systemd.enable = true;
    style = ''
      ${builtins.readFile "${pkgs.waybar}/etc/xdg/waybar/style.css"}

      window#waybar {
        background: transparent;
        border-bottom: none;
      }

      #workspaces button.active {
          background-color: #888888;
      }

      * {
        font-size: 18px;
      }
    '';
    settings = [
      {
        height = 30;
        layer = "top";
        position = "bottom";
        tray = {spacing = 5;};
        modules-center = ["hyprland/window"];
        modules-left = ["hyprland/workspaces"];
        modules-right = [
          "pulseaudio"
          "network"
          "cpu"
          "memory"
          "disk"
          "temperature"
          "battery"
          "power-profiles-daemon"
          "clock"
          "tray"
        ];
        power-profiles-daemon = {
          format = "{icon}";
          tooltip-format = "Profile: {profile}\nDriver: {driver}";
          format-icons = {
            power-saver = ""; # moon
            balanced = ""; # diamond
            performance = ""; # rocket
          };
        };
        battery = {
          format = "{capacity}% {icon}";
          format-alt = "{time} {icon}";
          format-charging = "{capacity}% ";
          format-icons = ["" "" "" "" ""];
          format-plugged = "{capacity}% ";
          states = {
            critical = 15;
            warning = 30;
          };
        };
        clock = {
          format-alt = "{:%Y-%m-%d}";
          tooltip-format = "{:%Y-%m-%d | %H:%M}";
          timezone = "US/Pacific";
        };
        cpu = {
          format = "{usage}% ";
          tooltip = false;
        };
        disk = {
          interval = 30;
          format = "{percentage_used}% 🖴";
          path = "/";
        };
        memory = {format = "{}% ";};
        network = {
          interval = 1;
          format-alt = "{ifname}: {ipaddr}/{cidr}";
          format-disconnected = "Disconnected ⚠";
          format-ethernet = "{ifname}: {ipaddr}/{cidr}   up: {bandwidthUpBits} down: {bandwidthDownBits}";
          format-linked = "{ifname} (No IP) ";
          format-wifi = "{essid} ({signalStrength}%) ";
        };
        pulseaudio = {
          format = "{volume}% {icon} {format_source}";
          format-bluetooth = "{volume}% {icon} {format_source}";
          format-bluetooth-muted = " {icon} {format_source}";
          format-icons = {
            car = "";
            default = ["" "" ""];
            handsfree = "";
            headphones = "";
            headset = "";
            phone = "";
            portable = "";
          };
          format-muted = " {format_source}";
          format-source = "{volume}% ";
          format-source-muted = "";
          on-click = lib.getExe pkgs.pwvucontrol;
        };
        temperature = {
          critical-threshold = 80;
          format = "{temperatureC}°C {icon}";
          format-icons = ["" "" ""];
        };
      }
    ];
  };
}
