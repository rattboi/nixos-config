{
  config,
  pkgs,
  lib,
  ...
}: {
  imports = [
    ./waybar.nix
    ./hyprshade.nix
  ];

  wayland.windowManager.hyprland = with lib; let
    hyper = "SUPER_ALT_CONTROL";
    alt_mod = "SUPER";
    terminal_cmd = getExe pkgs.ghostty;
    wofi = "${getExe pkgs.wofi} -I -G -i";
    menu_cmd = "${wofi} --show drun";
    brightnessctl = getExe pkgs.brightnessctl;
    playerctl = getExe pkgs.playerctl;
    spotifyctl = "${playerctl} --player=spotify";
    pactl = getExe' pkgs.pulseaudio "pactl";
    grimblast = getExe pkgs.grimblast;
    filemanager = "thunar";
    cliphist = getExe pkgs.cliphist;
    copymenu_cmd = "${cliphist} list | ${wofi} --show dmenu | ${cliphist} decode | ${getExe' pkgs.wl-clipboard "wl-copy"}";
  in {
    enable = true;
    xwayland.enable = true;

    settings = {
      "$mod" = "${hyper}";
      "$alt_mod" = "${alt_mod} SHIFT";
      "$terminal" = "${terminal_cmd}";
      "$menu" = "${menu_cmd}";
      "$copymenu" = "${copymenu_cmd}";
      "$notify" = "notify-send -h string:x-canonical-private-synchronous:hypr-cfg -u low";

      monitor = ",preferred,auto,1";

      misc = {
        disable_splash_rendering = true;
        force_default_wallpaper = 1;
      };

      gestures = {
        workspace_swipe = true;
        workspace_swipe_invert = false;
        workspace_swipe_forever = true;
      };

      exec-once = [
        "${getExe' pkgs.networkmanagerapplet "nm-applet"} --indicator"
        "${getExe' pkgs.wl-clipboard "wl-paste"} --watch cliphist store"
        "${getExe' pkgs.gnome-keyring "gnome-keyring-daemon"} --start --components=secrets"
      ];

      windowrulev2 = [
        "workspace 2,class:(firefox)"
        "workspace 3,class:(discord)"
        "workspace 4,class:(signal)"
        "workspace 4,class:(Mattermost)"
        "workspace 5,initialTitle:(Spotify)"
      ];

      animation = [
        "windows, 0"
        "layers, 0"
        "fade, 0"
        "border, 0"
        "borderangle, 0"
        "workspaces, 0"
      ];

      bindm = [
        "$mod, mouse:273, movewindow"
        "$mod, mouse:272, resizewindow"
      ];
      bind =
        [
          "$mod, P, exec, $menu"
          "$mod, O, exec, $copymenu"

          "$alt_mod, Return, exec, $terminal"
          "$alt_mod, M, exit,"
          "$mod, t, exec, ${filemanager}"

          # focus
          "$mod, h, movefocus, l"
          "$mod, l, movefocus, r"
          "$mod, k, movefocus, u"
          "$mod, j, movefocus, d"

          # move
          "$alt_mod, h, movewindow, l"
          "$alt_mod, l, movewindow, r"
          "$alt_mod, k, movewindow, u"
          "$alt_mod, j, movewindow, d"

          # resize
          "SUPER_CTRL, h, resizeactive, -50 0"
          "SUPER_CTRL, l, resizeactive, 50 0"
          "SUPER_CTRL, k, resizeactive, 0 50"
          "SUPER_CTRL, j, resizeactive, 0 -50"

          # fullscreen
          "$mod, f, fullscreen, 1"

          # switch workspace w/ mouse "scroll"
          "$mod, mouse_down, workspace, e+1"
          "$mod, mouse_up, workspace, e-1"

          "$mod, C, killactive,"

          # audio keys
          ",XF86AudioMute,        exec, ${pactl} set-sink-mute @DEFAULT_SINK@ toggle"
          ",XF86AudioLowerVolume, exec, ${pactl} set-sink-volume @DEFAULT_SINK@ -5%"
          ",XF86AudioRaiseVolume, exec, ${pactl} set-sink-volume @DEFAULT_SINK@ +5%"
          ",XF86AudioMicMute,     exec, ${pactl} set-source-mute @DEFAULT_SOURCE@ toggle"

          # brightness keys
          ",XF86MonBrightnessUp,   exec, ${brightnessctl} set 5%+"
          ",XF86MonBrightnessUp,   exec, $notify \"$(echo brightness: $(${brightnessctl} get))\""
          ",XF86MonBrightnessDown, exec, ${brightnessctl} set 5%-"
          ",XF86MonBrightnessDown, exec, $notify \"$(echo brightness: $(${brightnessctl} get))\""

          # screenshot
          "$mod, bracketright, exec, ${grimblast} copy screen"
          "$mod, backslash, exec, ${grimblast} copy area"

          # playerctl
          ",XF86AudioPause, exec, ${spotifyctl} play-pause"
          ",XF86AudioPlay, exec, ${spotifyctl} play-pause"
          ",XF86AudioPrev, exec, ${spotifyctl} previous"
          ",XF86AudioNext, exec, ${spotifyctl} next"
        ]
        ++ (
          # workspaces
          # binds $mod + [shift +] {1..10} to [move to] workspace {1..10}
          builtins.concatLists (builtins.genList (
              x: let
                ws = let
                  c = (x + 1) / 10;
                in
                  builtins.toString (x + 1 - (c * 10));
              in [
                "$mod,     ${ws},  workspace,             ${toString (x + 1)}"
                "$alt_mod, ${ws},  movetoworkspacesilent, ${toString (x + 1)}"
                "$alt_mod, f${ws}, focusworkspaceoncurrentmonitor, ${toString (x + 1)}"
              ]
            )
            10)
        );
    };
  };

  services.mako = {
    enable = true;
    backgroundColor = "#0A0E14";
    borderColor = "#53BDFA";
    defaultTimeout = 30 * 1000; # millis
    font = "monospace 10";
    icons = true;
    maxIconSize = 96;
    maxVisible = 3;
    sort = "-time";
    textColor = "#B3B1AD";
    width = 500;
  };

  services.hypridle = let
    idleScript = pkgs.writeShellScript "idle.sh" ''
      charging=$(acpi -a | grep Adapter | cut -d' ' -f 3)

      # only adjust brightness if not charging
      case "$charging" in
        "on-line")
          ;;
        "off-line")
          case "$1" in
            "on")
              brightnessctl -s set 10
              ;;
            "off")
              brightnessctl -r
              ;;
          esac
          ;;
      esac
    '';
  in {
    enable = true;

    settings = {
      listener = [
        {
          timeout = 90; # 1.5min.
          on-timeout = "${idleScript} on"; # set monitor backlight to minimum, avoid 0 on OLED monitor.
          on-resume = "${idleScript} off"; # monitor backlight restore.
        }
      ];
    };
  };
}
