{
  pkgs,
  lib,
  ...
}: {
  programs.vscode = {
    enable = true;
    extensions = with pkgs.vscode-extensions;
      [
        bbenoist.nix
        golang.go
        ms-python.python
        ms-azuretools.vscode-docker
        ms-vscode-remote.remote-ssh
        ms-vscode-remote.remote-containers
        rust-lang.rust-analyzer
        serayuzgur.crates
        tamasfe.even-better-toml
        vscodevim.vim
        mkhl.direnv
        dbaeumer.vscode-eslint
        bierner.docs-view
      ]
      ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "remote-ssh-edit";
          publisher = "ms-vscode-remote";
          version = "0.47.2";
          sha256 = "1hp6gjh4xp2m1xlm1jsdzxw9d8frkiidhph6nvl24d0h8z34w49g";
        }
        {
          name = "openscad-language-support";
          publisher = "Leathong";
          version = "1.2.5";
          sha256 = "17yqkl0yglxpsibjh3i50p4fg4bsfn61lnj49wjzclfxfl2z28pw";
        }
      ];
    userSettings = {
      # rust
      "[rust]"."editor.defaultFormatter" = "rust-lang.rust-analyzer";
      "rust-analyzer.check.command" = "clippy";
      "rust-analyzer.check.allTargets" = false;
      "rust-analyzer.cargo.features" = "all";

      # vim
      "vim.insertModeKeyBindings" = [
        {
          before = ["j" "j"];
          after = ["<Esc>"];
        }
      ];
      "vim.normalModeKeyBindings" = [
        {
          before = ["ctrl+p"];
          commands = ["workbench.action.quickOpen"];
          silent = true;
        }
        {
          before = ["t"];
          commands = ["go.test.cursor"];
          when = "editorLangId == go && editorTextFocus";
        }
        {
          before = ["T"];
          commands = ["go.test.workspace"];
          when = "editorLangId == go && editorTextFocus";
        }
      ];
    };
  };
}
