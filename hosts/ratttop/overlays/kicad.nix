{pkgs, ...}: final: prev: {
  kicad = prev.kicad.overrideAttrs (prevAttrs: {
    makeWrapperArgs =
      prevAttrs.makeWrapperArgs
      ++ [
        "--set GDK_BACKEND x11"
      ];
  });
}
