{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:
with lib; let
  cfg = config.modules.samba;
in {
  options.modules.samba = with types; {
    enable = mkEnableOption "samba";

    credentials_file = mkOption {
      type = uniq str;
    };
  };

  config = mkIf cfg.enable {
    fileSystems."/mnt/share" = {
      device = "//192.168.1.34/storage";
      fsType = "cifs";
      options = let
        automount_opts = "x-systemd.automount,noauto,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s,user,users";
      in ["${automount_opts},credentials=${cfg.credentials_file},uid=1000,gid=100"];
    };
  };
}
