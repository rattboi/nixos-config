{
  pkgs,
  pkgs-stable,
  nur,
  config,
  lib,
  ...
}: let
  my-packages = with pkgs; [
    mattermost-desktop
    signal-desktop
    discord
    spotify
    slack
    element-desktop
    transgui
    evince
    foliate
    # bitwig-studio
    steam
    fallout-ce

    (xfce.thunar.override {
      thunarPlugins = [
        xfce.thunar-archive-plugin
        xfce.thunar-volman
      ];
    })
    xfce.exo
    filezilla

    solvespace
    openscad
    kicad

    ghostty
    helix
    jujutsu
    zed-editor

    kitty
    bottles

    gnome-firmware
    pwvucontrol
    grimblast
    networkmanagerapplet
    system-config-printer
    hyprshade
    wdisplays
    cliphist
    devenv
  ];

  stable-packages = with pkgs-stable; [
    orca-slicer
    comical
    dsview
  ];
in {
  imports = [
    ./hyprland
    ./vscode.nix
    ../../modules/home/firefox
    ../../modules/home/headless
  ];

  home = {
    homeDirectory = "/home/${config.home.username}";
    persistence = {
      "/persist/home/${config.home.username}" = {
        directories = [
          "Downloads"
          "Music"
          "Pictures"
          "Documents"
          "Videos"
          "code"
          ".gnupg"
          ".ssh"
          ".local/share/keyrings"
          ".local/share/fallout-ce"
          ".local/share/firefoxpwa"
          ".local/share/fish"
          ".local/state/wireplumber"
          ".mozilla"
          ".config/Signal"
          ".config/Mattermost"
          ".config/Transmission Remote GUI"
          ".config/discord"
          ".config/spotify"
          ".config/OrcaSlicer"
          ".config/Slack"
          ".config/fish"
          ".config/helix"
          ".cache/spotify"
          ".cache/hyprland"
          ".config/kicad"
          ".config/Element"
          ".apio"
        ];
        allowOther = true;
      };
    };

    packages = my-packages ++ stable-packages;
    stateVersion = "23.11"; # Please read the comment before changing.
  };

  services = {
    ssh-agent.enable = true;
  };

  programs = {
    home-manager.enable = true;

    fish = {
      enable = true;
      plugins = with pkgs.fishPlugins; [
        {
          name = "tide";
          inherit (tide) src;
        }
      ];
      interactiveShellInit = ''
        # Fish configuration
        set fish_greeting ""
      '';
    };

    bottom = {
      enable = true;
    };

    helix = {
      enable = true;
      settings = {
        theme = "base16_transparent";
        editor = {
          line-number = "relative";
          lsp.display-messages = true;
        };
      };
    };

    ghostty = {
      enable = true;
      settings = {
        font-size = 15;
        window-theme = "dark";
        theme = "${pkgs.ghostty}/share/ghostty/themes/Abernathy";
        command = "fish";
        window-decoration = false;
        mouse-hide-while-typing = true;
      };
    };

    kitty = {
      enable = true;
      font = {
        name = "DejaVu Sans";
        size = 15.0;
      };
      shellIntegration = {
        enableBashIntegration = true;
        enableZshIntegration = true;
      };
    };
  };

  xdg.configFile."xfce4/helpers.rc".text = ''
    TerminalEmulator=ghostty
  '';

  dconf.settings = {
    "org/gnome/desktop/interface" = {
      color-scheme = "prefer-dark";
    };
  };

  gtk = {
    enable = true;
    theme = {
      package = pkgs.gnome-themes-extra;
      name = "Adwaita-dark";
    };
  };

  qt = {
    enable = true;
    platformTheme.name = "adwaita";
    style = {
      package = pkgs.adwaita-qt;
      name = "adwaita-dark";
    };
  };
}
