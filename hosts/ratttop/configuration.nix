# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).
{
  inputs,
  config,
  lib,
  pkgs,
  pkgs-stable,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./suspend-hibernate.nix
    ./samba.nix
    ./greetd.nix
  ];

  # enable flakes + nix unified command
  nix = {
    package = pkgs.nixVersions.stable;
    extraOptions = ''
      experimental-features = nix-command flakes
      trusted-users = root rattboi
    '';

    settings = {
      substituters = ["https://hyprland.cachix.org"];
      trusted-public-keys = ["hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="];
    };
  };

  # Use the systemd-boot EFI boot loader.

  boot = {
    loader = {
      systemd-boot.enable = true;
      systemd-boot.configurationLimit = 10;
      efi.canTouchEfiVariables = true;
    };

    kernelPackages = pkgs.linuxPackages_6_11;
  };

  networking.hostName = "ratttop";
  networking.networkmanager.enable = true;

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  #   useXkbConfig = true; # use xkb.options in tty.
  # };

  # Enable CUPS to print documents.
  services.printing = {
    enable = true;
    drivers = with pkgs; [
      samsung-unified-linux-driver
      gutenprint
      gutenprintBin
      hplip
      splix
    ];
  };

  services.avahi = {
    enable = true;
    nssmdns4 = true;
    openFirewall = true;
  };

  # Enable sound with pipewire.
  services.pulseaudio.enable = false;

  hardware.bluetooth.enable = true;
  hardware.bluetooth.powerOnBoot = true;

  hardware.graphics.enable = true;
  security.polkit.enable = true;

  security.rtkit.enable = true;

  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;

    #    extraConfig.pipewire."92-low-latency" = {
    #      "context.properties" = {
    #        "default.clock.rate" = 48000;
    #        "default.clock.quantum" = 32;
    #        "default.clock.min-quantum" = 32;
    #        "default.clock.max-quantum" = 32;
    #      };
    #    };
  };

  services.blueman.enable = true;

  # Copy the NixOS configuration file and link it from the resulting system
  # (/run/current-system/configuration.nix). This is useful in case you
  # accidentally delete configuration.nix.
  # system.copySystemConfiguration = true;

  # This option defines the first version of NixOS you have installed on this particular machine,
  # and is used to maintain compatibility with application data (e.g. databases) created on older NixOS versions.
  #
  # Most users should NEVER change this value after the initial install, for any reason,
  # even if you've upgraded your system to a new NixOS release.
  #
  # This value does NOT affect the Nixpkgs version your packages and OS are pulled from,
  # so changing it will NOT upgrade your system - see https://nixos.org/manual/nixos/stable/#sec-upgrading for how
  # to actually do that.
  #
  # This value being lower than the current NixOS release does NOT mean your system is
  # out of date, out of support, or vulnerable.
  #
  # Do NOT change this value unless you have manually inspected all the changes it would make to your configuration,
  # and migrated your data accordingly.
  #
  # For more information, see `man configuration.nix` or https://nixos.org/manual/nixos/stable/options#opt-system.stateVersion .
  system.stateVersion = "24.05"; # Did you read the comment?

  users.users."rattboi" = {
    isNormalUser = true;
    initialPassword = "1";
    extraGroups = [
      "networkmanager"
      "wheel"
      "docker"
      "dialout"
    ];
    shell = pkgs.zsh;
    packages = with pkgs; [
      sops
      ssh-to-age
    ];
  };

  # Fonts
  fonts.packages = with pkgs; [
    nerd-fonts.droid-sans-mono
    font-awesome
    dejavu_fonts
    noto-fonts
    noto-fonts-cjk-sans
    noto-fonts-emoji
    fira
  ];

  boot.initrd.postDeviceCommands = lib.mkAfter ''
    mkdir /btrfs_tmp
    mount /dev/root_vg/root /btrfs_tmp
    if [[ -e /btrfs_tmp/root ]]; then
        mkdir -p /btrfs_tmp/old_roots
        timestamp=$(date --date="@$(stat -c %Y /btrfs_tmp/root)" "+%Y-%m-%-d_%H:%M:%S")
        mv /btrfs_tmp/root "/btrfs_tmp/old_roots/$timestamp"
    fi

    delete_subvolume_recursively() {
        IFS=$'\n'
        for i in $(btrfs subvolume list -o "$1" | cut -f 9- -d ' '); do
            delete_subvolume_recursively "/btrfs_tmp/$i"
        done
        btrfs subvolume delete "$1"
    }

    for i in $(find /btrfs_tmp/old_roots/ -maxdepth 1 -mtime +30); do
        delete_subvolume_recursively "$i"
    done

    btrfs subvolume create /btrfs_tmp/root
    umount /btrfs_tmp
  '';

  fileSystems."/persist".neededForBoot = true;
  environment.persistence."/persist/system" = {
    hideMounts = true;
    directories = [
      "/etc/nixos"
      "/var/log"
      "/var/lib/bluetooth"
      "/var/lib/nixos"
      "/var/lib/systemd/coredump"
      "/var/lib/power-profiles-daemon"
      "/var/lib/fprint"
      "/var/lib/docker"
      "/etc/NetworkManager/system-connections"
      {
        directory = "/var/lib/colord";
        user = "colord";
        group = "colord";
        mode = "u=rwx,g=rx,o=";
      }
    ];
    files = [
      # "/etc/machine-id"
      # { file = "/var/keys/secret_file"; parentDirectory = { mode = "u=rwx,g=,o="; }; }
    ];
  };

  environment.persistence."/persist" = {
    users.rattboi = {
      directories = [
        ".local/share/bottles"
      ];
    };
  };

  environment.etc.machine-id.source = ./machine-id;

  environment.systemPackages = with pkgs; [
    vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    git
    jq
    bc
    unar
    killall
    pulseaudio # for pactl
    brightnessctl
    wl-clipboard
    acpi
    mosh

    bind.dnsutils
    cifs-utils

    k3s
    hyprland-qtutils
  ];

  environment.variables = {
    EDITOR = "vim";
  };

  environment.sessionVariables.NIXOS_OZONE_WL = "1";

  programs.nh = {
    enable = true;
    flake = "/home/rattboi/code/nixos-config";
  };

  programs.fuse.userAllowOther = true;
  programs.zsh.enable = true;
  programs.fish.enable = true;

  services.interception-tools = let
    itools = pkgs.interception-tools;
    itools-dual = pkgs.interception-tools-plugins.dual-function-keys;
  in {
    enable = true;
    plugins = [itools-dual];
    # requires explicit paths: https://github.com/NixOS/nixpkgs/issues/126681
    udevmonConfig = pkgs.lib.mkDefault ''
      - JOB: "${lib.getExe' itools "intercept"} -g $DEVNODE | ${lib.getExe itools-dual} -c /etc/interception/dual-function-keys/hyper-caps.yaml | ${lib.getExe' itools "uinput"} -d $DEVNODE"
        DEVICE:
          NAME: .*
    '';
  };

  environment.etc."interception/dual-function-keys/hyper-caps.yaml".source = pkgs.writeText "hyper-caps.yaml" ''
    MAPPINGS:
      - KEY: KEY_CAPSLOCK
        TAP: KEY_ESC
        HOLD: [ KEY_LEFTCTRL, KEY_LEFTMETA, KEY_LEFTALT, ]
  '';

  programs.hyprland.enable = true;
  programs.hyprland.package = inputs.hyprland.packages."${pkgs.system}".hyprland;

  programs.thunar.enable = true;
  programs.xfconf.enable = true;
  programs.file-roller.enable = true;

  programs.thunar.plugins = with pkgs.xfce; [
    thunar-archive-plugin
    thunar-volman
  ];

  services.gvfs = {
    enable = true;
    package = lib.mkForce pkgs.gnome.gvfs;
  };
  services.tumbler.enable = true;
  services.gnome.gnome-keyring.enable = true;

  services.udev.extraRules = ''
    # Pico-ICE
    SUBSYSTEM=="usb", ATTRS{idVendor}=="1209", ATTRS{idProduct}=="b1c0", MODE="0666"
    # Nintendo Switch
    SUBSYSTEM=="usb", ATTRS{idVendor}=="057e", ATTRS{idProduct}=="3000", MODE="0666"
    # picotool (pico in dfu mode)
    SUBSYSTEM=="usb", ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="000f", MODE="0666"
    # ST-link v2
    SUBSYSTEM=="usb", ATTRS{idVendor}=="0483", ATTRS{idProduct}=="3748", MODE="0666"
    # DSLogic U3Pro32
    SUBSYSTEM=="usb", ATTRS{idVendor}=="2a0e", ATTRS{idProduct}=="002c", MODE="0666"
    # pi debug probe
    SUBSYSTEM=="usb", ATTRS{idVendor}=="2e8a", ATTRS{idProduct}=="000c", MODE="0666"
  '';

  services.fwupd = {
    enable = true;
    extraRemotes = [
      "lvfs-testing"
    ];
  };

  xdg.portal = {
    enable = true;
    extraPortals = [
      pkgs.xdg-desktop-portal-gtk
    ];
  };

  virtualisation = {
    docker = {
      enable = true;
    };
  };

  services.nextjs-ollama-llm-ui = {
    enable = true;
    ollamaUrl = "http://192.168.1.34:11434";
  };

  modules.samba = {
    enable = true;
    credentials_file = config.sops.secrets."samba/mnt-share".path;
  };

  #  networking.firewall.allowedTCPPorts = [ 8081 ];
}
