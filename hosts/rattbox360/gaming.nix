{
  config,
  pkgs,
  ...
}: {
  environment.systemPackages = with pkgs; [
    gamemode
    protonup
    protonup-qt
    mangohud

    lutris
    bottles
  ];

  programs.steam = {
    enable = true;
    gamescopeSession = {
      enable = true;
    };
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    localNetworkGameTransfers.openFirewall = true;
  };
}
