{
  pkgs,
  config,
  lib,
  ...
}: let
  port = 17200;
  version = "latest";
in {
  virtualisation.oci-containers.containers = {
    koreader-sync-server = {
      image = "koreader/kosync:${version}";
      ports = [
        "0.0.0.0:${toString port}:${toString port}"
      ];
      volumes = [
        "/docker/koreader-sync-server/logs/app:/app/koreader-sync-server/logs:rw"
        "/docker/koreader-sync-server/logs/redis:/var/log/redis:rw"
        "/docker/koreader-sync-server/data/redis:/var/lib/redis:rw"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "kosync.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString port}";
      };
    };
  };

  networking.firewall.allowedTCPPorts = [port];
}
