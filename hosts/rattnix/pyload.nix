{
  pkgs,
  config,
  lib,
  ...
}: {
  virtualisation.oci-containers.containers = {
    pyload-ng = {
      image = "lscr.io/linuxserver/pyload-ng:version-0.5.0b3.dev78";
      ports = [
        "0.0.0.0:8000:8000"
      ];
      environment = {
        PUID = "1000";
        PGID = "100";
        TZ = "America/Vancouver";
      };
      volumes = [
        "/docker/pyload/config:/config"
        "/mnt/storage/pyload:/downloads"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "pyload.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:8000";
      };
    };
  };
}
