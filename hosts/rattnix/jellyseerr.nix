{
  pkgs,
  config,
  lib,
  ...
}: let
  port = "5055";
  version = "1.9.2";
in {
  virtualisation.oci-containers.containers = {
    jellyseerr = {
      image = "fallenbagel/jellyseerr:${version}";
      ports = [
        "0.0.0.0:${port}:${port}"
      ];
      environment = {
        PUID = "1000";
        PGID = "100";
        LOGLEVEL = "debug";
        TZ = "America/Vancouver";
      };
      volumes = [
        "/docker/jellyseerr/config:/app/config"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "jellyseerr.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${port}";
      };
    };
  };
}
