{
  pkgs,
  config,
  lib,
  ...
}: let
  web_port = 9999;
  ports = [
    "${toString web_port}:9090"
    "1883:1883"
    "7070:7070"
    "5683-5688:5683-5688/udp"
  ];
  version = "latest";
in {
  virtualisation.oci-containers.containers = {
    thingsboard = {
      image = "thingsboard/tb-postgres:${version}";
      ports = ports;
      environment = {
        TB_QUEUE_TYPE = "in-memory";
      };
      volumes = [
        "/docker/thingsboard/data:/data"
        "/docker/thingsboard/logs:/var/log/thingsboard"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "thingsboard.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString web_port}";
        proxyWebsockets = true;
      };
    };
  };

  networking.firewall.allowedTCPPorts = [web_port];
}
