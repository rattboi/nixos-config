{
  pkgs,
  config,
  lib,
  ...
}: {
  programs.msmtp = {
    enable = true;
    setSendmail = true;

    defaults = {
      auth = true;
      tls = true;
      logfile = "~/.msmtp.log";
    };

    accounts = {
      "gmail" = {
        host = "smtp.gmail.com";
        port = 587;
        from = "rattboi@gmail.com";
        user = "rattboi";
        passwordeval = "cat ${config.sops.secrets.mailpass.path}";
      };
    };

    extraConfig = "account default : gmail";
  };
}
