{config, ...}: {
  services.home-assistant = {
    config = {
      sensor = [
        # Weather prediction
        {
          platform = "nws";
          name = "weather";
        }
      ];

      sun = {};

      group.today.entities = ["sensor.weather_temperature" "sun.sun"];
    };
  };
}
