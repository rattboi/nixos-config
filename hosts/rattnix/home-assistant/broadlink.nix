{
  pkgs,
  config,
  lib,
  ...
}: let
  remote_command = {
    command,
    id ? "remote.bedroom_remote",
  }: {
    service = "remote.send_command";
    data = {
      entity_id = id;
      command = command;
    };
  };

  remote_script = {
    s_alias,
    s_command,
    remote_entity_id ? "remote.bedroom_remote",
  }: {
    alias = s_alias;
    sequence = [
      (remote_command {
        command = s_command;
        id = remote_entity_id;
      })
    ];
  };

  get_command = {
    s_alias,
    s_command,
  }:
    s_command;

  small_delay = {
    delay = {
      milliseconds = 200;
    };
  };

  # HDMI Switch
  hdmi_switch_on = {
    s_alias = "HDMI Switch On";
    s_command = "b64:JgBQAAABKZQSOBITEhIUEhITEhMTEhITEhMTNxM3EjgTNxI4EjcUNxISExMSNxM4EjcTExI3EhQROBI4ExISExQREzcUERM3EwAFHAABKUoWAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_off = {
    s_alias = "HDMI Switch Off";
    s_command = "b64:JgBQAAABKZQUNRUQEhMWDxUQEhMWDxUQEhMWNBI4FTUWNBI3FTUWNBI4FTUWNBQ2FDUVEBUREhIVEBUREhIWDxUREjcVNRU1EgAFHQABKEoXAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_1 = {
    s_alias = "HDMI Switch Input 1";
    s_command = "b64:JgBQAAABLJETNhMTEhIWDxMTEhIWDxMTEhIWNRM2FDYWNBI4FDUWNRI3EzcWDxM3FjQSExYPExISExYPEzcWDxMSEjgTNxU1FQAFGgABKUkWAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_2 = {
    s_alias = "HDMI Switch Input 2";
    s_command = "b64:JgBQAAABLZASOBUQEhMWDxUQEhMWDxUQEhMWNBY0EzYXNBQ1EzcWNBITFjQWNBM3FjQSExYPFRASOBUQEhMWDxMSEjgUNRc0EgAFHQABLEcWAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_3 = {
    s_alias = "HDMI Switch Input 3";
    s_command = "b64:JgBQAAABKJQSOBUQEhMVEBMSFBEVEBITFBEVNRI4EjcVNhI3EjgVNRI4FBESOBQ1FREUERISFRESExI3FRESEhU1FTUSOBU1EgAFHQABKUkXAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_4 = {
    s_alias = "HDMI Switch Input 4";
    s_command = "b64:JgBQAAABLJESOBITEhMWDxMSEhMWDxUQEhMWNBI4FDUXNBI3EzcWNBITFjQUERYPEjgWDxITEhMWNBUQFjQSOBMSEjgSNxc0EgAFHQABLEYXAA0FAAAAAAAAAAA=";
  };
  hdmi_switch_5 = {
    s_alias = "HDMI Switch Input 5";
    s_command = "b64:JgBQAAABKZQUNRURFBATExUQFBATExUQFBAUNxU0FTUTNxU1FTUTNxUQExIUERUQEzcVDxQSFBEVNRQ1EzgUNRUQFTUVNRM3FQAFGgABK0gVAA0FAAAAAAAAAAA=";
  };

  # Motorized Projection Screen
  screen_up = {
    s_alias = "Projector Screen Up";
    s_command = "b64:sjQ0AA4qDSoOKSkPDgABoykPDioOKg4qKQ8NKg0qDiopDykPKQ8OKg4qKQ8OKg4qKQ8NKikPDioAAAAA";
  };
  screen_down = {
    s_alias = "Projector Screen Down";
    s_command = "b64:siY0ACkPDioOKikPDSopDw4qDSopDw4qDioOAAGjKQ8OKg4qDiopDw0qDSoOKikPKQ8pDw0qDikAAAAA";
  };
  screen_stop = {
    s_alias = "Projector Screen Stop";
    s_command = "b64:shQ0ACgPDSoOKg0qKBAoDygQDSoNKigPDSoNKigPDSooDw0qDSoNKigPDSoNAAGkKA8NKg0qDSoAAAAA";
  };

  # Soundbar
  soundbar_power = {
    s_alias = "Soundbar Power";
    s_command = "b64:JgBWAJaUFBITEhQ2EzgUEhM3FBITEhQSExIUNhU2FBITNxQSExIUEhM3FDYVNhQ2ExMUEhMSFDYUEhQSFBEUEhQ2FDcUNhQABfuVlRQ2EwAMZZiRFDcUAA0FAAA=";
  };
  soundbar_up = {
    s_alias = "Soundbar Volume Up";
    s_command = "b64:JgBOAJeSFhAVERU1FTUWEBU1FhAVERUQFREVNRU1FhAVNRYQFREVNRU2FTUVERU1FREVEBURFRAVERUQFTYVEBU2FTUVNhUABfqXkhY1FQANBQAAAAAAAAAAAAA=";
  };
  soundbar_down = {
    s_alias = "Soundbar Volume Down";
    s_command = "b64:JgBOAJKTEhMTERQ1EjgSEhI3EhMRExISEhISOBE4EhISOBETEhISEhI4EjcSEhI4EhISEhISFDYSEhISEjgSEhQ1EjgROBIABgKVkRI4FAANBQAAAAAAAAAAAAA=";
  };
  soundbar_mute = {
    s_alias = "Soundbar Mute";
    s_command = "b64:JgBOAJiSFBEVERU1EzgVEBU2EhMVERUQFREVNRU2FRAVNhUQFREVNRU2FTUVNhU1FREVEBURFRAVERUQFREVEBU2FTUVNhUABfqXkhY1FQANBQAAAAAAAAAAAAA=";
  };

  # Projector
  projector_power = {
    s_alias = "Projector Power";
    s_command = "b64:JgB4AAABKJMTNxM3ExMUNhQ2EzcTExMTEhMTEhM4EhMTEhMTEzcTNxMTExITExITExMSExMSExMSOBM3EzcTOBM3EzcTNxM4EwAFHwABKEoTAAxgAAEpSRMADGEAAShKFAAMYAABKUkUAAxfAAEqSRQADF8AAShKFAANBQ==";
  };
  projector_up = {
    s_alias = "Projector Up";
    s_command = "b64:JgBoAAABKZIUNhQ3FBEUNhQ2EzgTEhQSEhMTExI4FBETExITEzcUNhQ3FBETOBM3FBETExITExMSExM3FBITEhM3FDYUNxQ2FAAFHgABKEoUAAxfAAEoShQADF8AAShKFAAMXwABKUkUAA0F";
  };
  projector_down = {
    s_alias = "Projector Down";
    s_command = "b64:JgBgAAABKpETNxM4ExITNxM4EjgTEhMTExITExI4ExIVERMSFTUTOBI4ExITOBITEzcTExMSExMSExM3ExMTNxMSEzgTNxM3EwAFIAABKEoTAAxhAAEpSRQADGAAASlJFAANBQAAAAAAAAAA";
  };
  projector_left = {
    s_alias = "Projector Left";
    s_command = "b64:JgBYAAABKZETNxU2FBEVNRU1FTYUERUQFREVEBM4FBEVEBURFTUVNRURFBETExITFTUTExMSFRATOBU1FTUVNRMTEzcTNxM4EwAFHwABKkgUAAxeAAEoShIADQU=";
  };
  projector_right = {
    s_alias = "Projector Right";
    s_command = "b64:JgBYAAABKJITOBQ2FBETOBM3EzcTExMSExITExM3FBISExMSEzcUNxQRFDYUEhQRFDcUERMTEhMUNhQSEzcUNhQSEzcUNhQ2FAAFHgABKEoTAAxfAAEpSRQADQU=";
  };
  projector_ok = {
    s_alias = "Projector Ok";
    s_command = "b64:JgBgAAABKJITOBI4ExITNxM4EzcTEhMTExITExM3ExITExMSEzgSOBM3ExMSExMTEjgTEhMTEhMTExI4EzcTNxMTEzcTNxM4EwAFHwABKEoTAAxhAAEoShMADGEAAShKEwANBQAAAAAAAAAA";
  };
  projector_back = {
    s_alias = "Projector Back";
    s_command = "b64:JgBwAAABKJITOBM3ExITOBI4EzcTExITExITExM3ExITExMSEzcTOBMSEzcTOBM3ExITExMSExMTNxMTEhMTEhM3EzgTNxM3EwAFHwABKEoTAAxgAAEqSBMADF8AAStHFQAMXgABK0YVAAxeAAEqSBUADQUAAAAAAAAAAA==";
  };
  projector_menu = {
    s_alias = "Projector Menu";
    s_command = "b64:JgBQAAABKJMTNxM3ExMTNxM3EzcTExMTEhMTEhM3ExMTEhMTEjgTNxMTEhMTNxM3ExMTEhMTEhMTNxM3ExMTEhM4EjgTNxM3EwAFHwABKEoTAA0FAAAAAAAAAAA=";
  };
  projector_home = {
    s_alias = "Projector Home";
    s_command = "b64:JgBYAAABKpATOBQ2FBEVNhM3FDYTExQRFREUERQ2FREUERUQFTYUNhUQFREUNhURFDYUERURFBEVNRU2FBEVNRURFDYVNRU2FAAFHgABKkgTAAxiAAEoShMADQU=";
  };
  projector_mode = {
    s_alias = "Projector Mode";
    s_command = "b64:JgBQAAABKZIUNRU2FBIUNhQ2FDcUERQSExIUERQ2FREUEhMSFDYUNhQSFDYUEhQRFBITEhQRFBIUNhQSEzcUNhQ2FDYVNhQ2FAAFHwABKUkVAA0FAAAAAAAAAAA=";
  };

  script = {
    hdmi_switch_on = remote_script hdmi_switch_on;
    hdmi_switch_off = remote_script hdmi_switch_off;
    hdmi_switch_1 = remote_script hdmi_switch_1;
    hdmi_switch_2 = remote_script hdmi_switch_2;
    hdmi_switch_3 = remote_script hdmi_switch_3;
    hdmi_switch_4 = remote_script hdmi_switch_4;
    hdmi_switch_5 = remote_script hdmi_switch_5;

    # Motorized Projection Screen
    screen_up = remote_script screen_up;
    screen_down = remote_script screen_down;
    screen_stop = remote_script screen_stop;

    # Soundbar
    soundbar_power = remote_script soundbar_power;
    soundbar_up = remote_script soundbar_up;
    soundbar_down = remote_script soundbar_down;
    soundbar_mute = remote_script soundbar_mute;

    # Projector
    projector_power = remote_script projector_power;
    projector_up = remote_script projector_up;
    projector_down = remote_script projector_down;
    projector_left = remote_script projector_left;
    projector_right = remote_script projector_right;
    projector_ok = remote_script projector_ok;
    projector_back = remote_script projector_back;
    projector_menu = remote_script projector_menu;
    projector_home = remote_script projector_home;
    projector_mode = remote_script projector_mode;

    tv_on = {
      alias = "TV On";
      sequence = [
        (remote_command {command = get_command projector_power;})
        small_delay
        (remote_command {command = get_command hdmi_switch_1;})
        {delay = 30;}
        (remote_command {command = get_command projector_ok;})
        small_delay
        (remote_command {command = get_command soundbar_power;})
      ];
    };

    tv_off = {
      alias = "TV Off";
      sequence = [
        (remote_command {command = get_command soundbar_power;})
        small_delay
        (remote_command {command = get_command projector_power;})
      ];
    };

    screen_down_full = {
      alias = "Screen Down";
      sequence = [
        (remote_command {command = get_command screen_down;})
        {delay = 30;}
        (remote_command {command = get_command screen_stop;})
      ];
    };

    screen_up_full = {
      alias = "Screen Up";
      sequence = [
        (remote_command {command = get_command screen_up;})
        {delay = 30;}
        (remote_command {command = get_command screen_stop;})
      ];
    };
  };
in {
  services.home-assistant = {
    config = {
      script = script;
    };
  };
}
