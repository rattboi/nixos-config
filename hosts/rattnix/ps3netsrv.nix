{
  pkgs,
  config,
  lib,
  ...
}: {
  virtualisation.oci-containers.containers = {
    ps3netsrv = {
      image = "shawly/ps3netsrv:v1.6.0";
      ports = [
        "38008:38008"
      ];
      environment = {
        USER_ID = "1000";
        GROUP_ID = "100";
      };
      volumes = [
        "/mnt/storage/nfs_backup/Games/PS3/ps3netsrv:/games:rw"
      ];
    };
  };

  networking.firewall.allowedTCPPorts = [38008];
}
