{
  pkgs,
  nur,
  config,
  lib,
  ...
}: {
  imports = [
    ../../modules/home/headless
  ];

  home = {
    homeDirectory = "/home/${config.home.username}";
    stateVersion = "23.11"; # Please read the comment before changing.
  };

  programs = {
    home-manager.enable = true;
  };
}
