{
  pkgs,
  config,
  lib,
  ...
}: {
  services.nginx.virtualHosts = {
    "games.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        root = "/mnt/storage/transmission/games";
        extraConfig = ''
          allow 192.168.1.1/24;
          deny all;
          autoindex on;
        '';
      };
    };
  };
}
