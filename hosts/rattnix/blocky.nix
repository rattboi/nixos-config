{
  pkgs,
  config,
  lib,
  ...
}: {
  environment.systemPackages = with pkgs; [
    # install as system package as well so we can use it's cli against the server
    blocky
  ];

  services.blocky = {
    enable = true;
    settings = {
      ports.dns = 53;
      ports.http = 4000;
      connectIPVersion = "v4";

      upstreams.groups.default = [
        "https://one.one.one.one/dns-query"
      ];

      bootstrapDns = {
        upstream = "https://one.one.one.one/dns-query";
        ips = ["1.1.1.1" "1.0.0.1"];
      };

      blocking = {
        blackLists = {
          ads = [
            "https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt"
            "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts"
            "https://sysctl.org/cameleon/hosts"
            "https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt"
            ''
              # inline
              afs.googlesyndication.com

              adm.hotjar.com
              identify.hotjar.com
              insights.hotjar.com
              surveys.hotjar.com

              events.reddit.com

              ads-api.tiktok.com
              ads-sg.tiktok.com
              analytics-sg.tiktok.com
              business-api.tiktok.com

              analyticsengine.s3.amazonaws.com
              analytics.s3.amazonaws.com
              tools.mouseflow.com
              cdn-test.mouseflow.com
              freshmarketer.com
              claritybt.freshmarketer.com
              fwtracks.freshmarketer.com
              api.luckyorange.com
              realtime.luckyorange.com

              udc.yahoo.com
              udcm.yahoo.com
              log.fc.yahoo.com
              log.byteoversea.com
              adtech.yahooinc.com

              api.bugsnag.com
              app.bugsnag.com
              browser.sentry-cdn.com
              app.getsentry.com

              bdapi-ads.realmemobile.com
              bdapi-in-ads.realmemobile.com
              ck.ads.oppomobile.com
              click.oneplus.cn
              api-adservices.apple.com
              books-analytics-events.apple.com
              data.mistat.india.xiaomi.com
              data.mistat.rus.xiaomi.com
              samsung-com.112.2o7.net
              analytics-api.samsunghealthcn.com

              metrics.data.hicloud.com
              metrics2.data.hicloud.com
              grs.hicloud.com
              logservice.hicloud.com
              logservice1.hicloud.com
              logbak.hicloud.com

              extmaps-api.yandex.net
              adfstat.yandex.ru
              metrika.yandex.ru
              offerwall.yandex.net
              logbak.hicloud.com
              adfox.yandex.ru
            ''
          ];
        };

        clientGroupsBlock = {
          default = ["ads"];
        };
      };
    };
  };

  networking.firewall.allowedUDPPorts = [53];
  networking.firewall.allowedTCPPorts = [53];
}
