# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
#
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  inputs,
  config,
  pkgs,
  pkgs-unstable,
  ...
}: {
  imports = [
    # Include the results of the hardware scan.
    ./hardware-configuration.nix
    ./mail.nix
    ./pyload.nix
    ./ps3netsrv.nix
    ./airdcppd.nix
    # ./home-assistant.nix
    ./samba.nix
    ./jellyseerr.nix
    ./games-server.nix
    ./blocky.nix
    ./storyteller.nix
    ./koreader-sync-server.nix
    # ./kapowarr.nix
    # ./thingsboard.nix
    ../../modules/snapraid-runner.nix
    # ../../modules/monitoring.nix
    ../../modules/dyndns.nix
    ../../modules/bucket-server.nix
    ../../modules/unpackerr.nix
    ../../modules/transmission.nix
    ../../modules/media-pipeline.nix
    ../../modules/1001bot.nix
    ../../modules/nytimes-games-bot.nix
    ../../modules/jellyfin.nix
  ];

  nix = {
    settings = {
      experimental-features = [
        "nix-command"
        "flakes"
      ];
    };
  };

  nixpkgs.config.allowUnfree = true;

  security.acme = {
    acceptTerms = true;
    defaults = {
      dnsProvider = "porkbun";
      credentialsFile = config.sops.secrets."porkbun/credentials_file".path;
      email = "bradon@kanyid.org";
    };
  };
  security.polkit.enable = true;

  services.scrutiny = {
    enable = true;
    openFirewall = true;
    collector.enable = true;
  };

  services.ollama = {
    enable = true;
    package = pkgs-unstable.ollama;
    acceleration = "cuda";
    host = "[::]";
    port = 11434;
  };

  services.dyndns = {
    enable = true;
    baseDomain = config.networking.domain;
    subDomains = builtins.attrNames config.services.nginx.virtualHosts;
    ttl = 600;
    secretPath = config.sops.secrets."porkbun/credentials_file".path;
    startAt = "*-*-* *:0/5:00";
  };

  services."1001bot" = {
    enable = true;
    username = "1001-bot";
    albumgeneratorUrl = "https://1001albumsgenerator.com/api/v1/groups/heavy-1001-is-heavy";
    webhookSecretFile = config.sops.secrets."1001bot/webhook_url".path;
  };

  services.nytimesgamesbot = {
    enable = true;
    webhookSecretFile = config.sops.secrets."nytimesgamesbot/webhook_url".path;
  };

  services.media-pipeline = {
    enable = true;

    unpackerrEnvironmentFile = config.sops.secrets."unpackerr/environmentFile".path;

    # makes it simpler for unpackerr to interact w/ recent torrents
    unpackerrUser = "transmission";
    unpackerrGroup = "transmission";
  };

  services.snapraid-runner = {
    enable = true;
    startAt = "*-*-* 6:30:00";
    snapraidConfig = builtins.readFile ./snapraid.conf;
    emailAddress = "rattboi@gmail.com";
    deleteThreshold = 1500;
  };

  modules.transmission = {
    enable = true;

    download_dir = "/mnt/storage/transmission";
    blocklist_url = "https://github.com/Naunter/BT_BlockLists/raw/master/bt_blocklists.gz";
    credentials_file = config.sops.secrets."transmission/credentials.json".path;
  };

  modules.jellyfin = {
    enable = true;
  };

  boot.vesa = false;

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.grub.splashImage = null;

  systemd.enableEmergencyMode = false;

  networking = {
    hostName = "home";
    domain = "kanyid.org";

    useDHCP = false;
    enableIPv6 = false;

    # since we have 1 nic, use old naming scheme
    # this ends up being more stable, because the nic name changed
    # when I added a video card
    usePredictableInterfaceNames = false;
    interfaces.eth0.useDHCP = true;

    firewall.allowedTCPPorts = [
      80 # http
      443 # https
      11434 # ollama
    ];
  };

  # Select internationalisation properties.
  # i18n.defaultLocale = "en_US.UTF-8";
  # console = {
  #   font = "Lat2-Terminus16";
  #   keyMap = "us";
  # };

  # Set your time zone.
  time.timeZone = "US/Pacific";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    wget
    zsh
    git
    vim_configurable
    bc
    htop
    mosh
    tmux
    fzf
    glances
    internetarchive
    bind.dnsutils

    python3Full
    python3Packages.virtualenvwrapper

    gptfdisk
    xfsprogs
    snapraid
    mergerfs
    mergerfs-tools
    smartmontools
    ddrescue

    rclone

    sops
    ssh-to-age
  ];

  programs.nh = {
    enable = true;
    flake = "/home/rattboi/code/nixos-config";
  };

  programs.mosh.enable = true;
  programs.fuse.userAllowOther = true;

  services = {
    bucket-server = let
      bucket_name = "books.kanyid.org";
      aws_region = "us-west-2";
    in {
      enable = true;

      syncDirs = {
        "/mnt/storage/comics" = "comics";
        "/mnt/storage/books" = "books";
      };

      bucketName = bucket_name;
      bucketUrl = "https://s3.${aws_region}.amazonaws.com/${bucket_name}";

      globalFlags = "-v";

      awsAccessKeyIdFile = config.sops.secrets."bucket_server/aws_access_key_id".path;
      awsSecretAccessKeyFile = config.sops.secrets."bucket_server/aws_secret_access_key".path;
      awsDefaultRegion = aws_region;
    };

    openssh.enable = true;

    fstrim.enable = true;

    nginx.enable = true;

    apcupsd.enable = true;

    locate.enable = true;

    postgresql = {
      enable = true;
      package = pkgs.postgresql_15;
    };
  };

  services.nginx.virtualHosts = {
    "${config.networking.hostName}.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;
    };
  };

  virtualisation = {
    docker = {
      enable = true;
    };
  };

  ### MERGERFS POOL BEGIN

  fileSystems."/mnt/parity0" = {
    device = "/dev/disk/by-uuid/9d432ed1-d535-4ebd-b367-2260345acef0";
    fsType = "xfs";
  };

  fileSystems."/mnt/parity1" = {
    device = "/dev/disk/by-uuid/4de986b5-b90e-4e3f-8da7-988264238fb5";
    fsType = "xfs";
  };

  fileSystems."/mnt/disk0" = {
    device = "/dev/disk/by-uuid/0dce3835-8d23-446e-8eb3-d088ef068491";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk1" = {
    device = "/dev/disk/by-uuid/60c819b2-9473-47a2-a302-a2dcefc0c7bb";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk2" = {
    device = "/dev/disk/by-uuid/cb8d6dee-d27d-4f01-9be9-82f622c41ca5";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk3" = {
    device = "/dev/disk/by-uuid/82f415d9-05df-4ecd-aae7-44be421c24db";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk4" = {
    device = "/dev/disk/by-uuid/6a80f9bf-7e15-41e7-ba21-fe6c866a157b";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk5" = {
    device = "/dev/disk/by-uuid/e87b442f-33eb-416a-8228-7b1a88c7578d";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/disk6" = {
    device = "/dev/disk/by-uuid/93129074-c70d-425b-9330-a6f78aef59ed";
    fsType = "xfs";
    options = ["nofail"];
  };

  fileSystems."/mnt/storage" = {
    device = "/mnt/disk*";
    fsType = "fuse.mergerfs";
    options = [
      "defaults"
      "allow_other"
      "nonempty"
      "minfreespace=20G"
      "fsname=mergerfs"
      "category.create=mfs"
      "use_ino"
      "cache.files=off"
      "dropcacheonclose=true"
      "noforget"
      "inodecalc=path-hash"
      "x-systemd.requires=mnt-disk0.mount"
      "x-systemd.requires=mnt-disk1.mount"
      "x-systemd.requires=mnt-disk2.mount"
      "x-systemd.requires=mnt-disk3.mount"
      "x-systemd.requires=mnt-disk4.mount"
      "x-systemd.requires=mnt-disk5.mount"
      "x-systemd.requires=mnt-disk6.mount"
    ];
  };

  ### MERGERFS POOL END

  users.extraUsers.rattboi = {
    name = "rattboi";
    group = "users";
    isNormalUser = true;
    extraGroups = [
      "wheel"
      "disk"
      "systemd-journal"
      "docker"
      "transmission"
    ];
    createHome = true;
    uid = 1000;
    home = "/home/rattboi";
    shell = "/run/current-system/sw/bin/zsh";
  };

  # Enable OpenGL
  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  hardware.nvidia = {
    modesetting.enable = true;
    powerManagement.enable = false;
    powerManagement.finegrained = false;
    open = false;
    nvidiaSettings = false;
    package = config.boot.kernelPackages.nvidiaPackages.stable;
  };

  hardware.nvidia-container-toolkit.enable = true;

  services.xserver.videoDrivers = ["nvidia"];

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "20.03"; # Did you read the comment?
}
