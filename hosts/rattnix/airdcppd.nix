{
  pkgs,
  config,
  lib,
  ...
}: {
  virtualisation.oci-containers.containers = {
    airdcpp = {
      image = "gangefors/airdcpp-webclient";
      ports = [
        "5600:5600" # ui
        "24830:24830" # transfer
        "24830:24830/udp" # search
        "30501:30501" # encrypted transfer
      ];
      environment = {
        PUID = "1000";
        PGID = "131";
        TZ = "America/Vancouver";
      };
      volumes = [
        "/opt/airdcpp-web:/.airdcpp"
        "/mnt/storage/comics:/mnt/storage/comics"
        "/mnt/storage/new_torrents/comics:/mnt/storage/new_torrents/comics"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "airdcpp.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:5600";
        proxyWebsockets = true;
      };
    };
  };
}
