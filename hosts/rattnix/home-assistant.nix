{
  pkgs,
  config,
  lib,
  ...
}: let
  # replace this once this is rolled into unstable or a proper release
  # needed for newer broadlink plugin
  hass_lat_file = "/var/secret/hass_latitude.txt";
  hass_long_file = "/var/secret/hass_longitude.txt";
in {
  imports = [
    ./home-assistant/broadlink.nix
  ];

  services.home-assistant = {
    enable = true;
    openFirewall = false;

    extraComponents = ["broadlink" "cloud" "mobile_app" "zeroconf" "met" "ssdp" "default_config" "upnp" "denonavr" "heos" "spotify" "ipp" "simplepush"];

    config = {
      # turn on to edit GUI
      lovelace = {};

      homeassistant = {
        name = "Home";
        latitude = builtins.readFile hass_lat_file;
        longitude = builtins.readFile hass_long_file;
        elevation = 171;
        unit_system = "imperial";

        time_zone = config.time.timeZone;
        #        auth_providers = [{
        #          type = "trusted_networks";
        #          trusted_networks = [
        #            "127.0.0.1/32"
        #          ];
        #        }];
      };

      # needed, don't know why
      default_config = {};
      config = {};
    };
  };

  services.nginx.virtualHosts = {
    "hass.${config.networking.hostName}.${config.networking.domain}" = {
      forceSSL = true;
      enableACME = true;
      acmeRoot = null;

      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.home-assistant.config.http.server_port}";
        proxyWebsockets = true;
      };
    };
  };
}
