{
  pkgs,
  config,
  lib,
  ...
}: let
  port = 8001;
  version = "web-v1.1.3";
in {
  virtualisation.oci-containers.containers = {
    storyteller = {
      image = "registry.gitlab.com/smoores/storyteller:${version}";
      ports = [
        "0.0.0.0:${toString port}:${toString port}"
      ];
      environment = {
        STORYTELLER_DEVICE = "cuda";
      };
      environmentFiles = [
        config.sops.secrets."storyteller/environmentFile".path
      ];
      volumes = [
        "/docker/storyteller:/data:rw"
        "/mnt/storage/:/storage:ro"
      ];
      extraOptions = [
        "--device=nvidia.com/gpu=0"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "storyteller.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString port}";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header x-forwarded-host "storyteller.${config.networking.hostName}.${config.networking.domain}";
          proxy_read_timeout 3600;
          client_max_body_size 1000M;
          client_body_buffer_size 50M;
        '';
      };
    };
  };

  networking.firewall.allowedTCPPorts = [port];
}
