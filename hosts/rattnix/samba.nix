{
  pkgs,
  config,
  lib,
  ...
}: let
  sambaShareDefaults = {
    "browseable" = "yes";
    "read only" = "yes";
    "guest ok" = "yes";
    "create mask" = "0644";
    "directory mask" = "0755";
    "force user" = config.users.extraUsers.rattboi.name;
    "force group" = config.users.extraUsers.rattboi.group;
  };
in {
  services.samba = {
    enable = true;
    openFirewall = true;
    settings = {
      global = {
        "workgroup" = "WORKGROUP";
        "server string" = "smbnix";
        "netbios name" = "smbnix";
        "security" = "user";
        "hosts allow" = "127.0.0.1 192.168.1.0/24";
        "hosts deny" = "0.0.0.0/0";
        "guest account" = "nobody";
        "map to guest" = "bad user";
        "security type" = "user";
      };

      storage = lib.mergeAttrs sambaShareDefaults {
        path = "/mnt/storage";
      };

      storage_rw = lib.mergeAttrs sambaShareDefaults {
        path = "/mnt/storage";
        "read only" = "no";
      };
    };
  };

  networking.firewall.allowPing = true;
  networking.firewall.allowedTCPPorts = [445 139];
  networking.firewall.allowedUDPPorts = [137 138];
}
