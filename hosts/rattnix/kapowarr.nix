{
  pkgs,
  config,
  lib,
  ...
}: let
  port = 5656;
  version = "nightly";
in {
  virtualisation.oci-containers.containers = {
    kapowarr = {
      image = "kapowarr:${version}";
      ports = [
        "0.0.0.0:${toString port}:${toString port}"
      ];
      environment = {
        PUID = "1000";
        PGID = "100";
        LOGLEVEL = "debug";
        TZ = "America/Vancouver";
      };
      volumes = [
        "/docker/kapowarr/db:/app/db"
        "/docker/kapowarr/temp:/app/temp_downloads"
        "/docker/kapowarr/content:/content"
      ];
    };
  };

  services.nginx.virtualHosts = {
    "kapowarr.${config.networking.hostName}.${config.networking.domain}" = {
      enableACME = true;
      forceSSL = true;
      acmeRoot = null;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString port}";
        proxyWebsockets = true;
      };
    };
  };

  networking.firewall.allowedTCPPorts = [port];
}
