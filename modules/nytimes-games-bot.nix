{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.services."nytimesgamesbot";
in {
  options.services."nytimesgamesbot" = with lib;
  with types; {
    enable = mkEnableOption "nytimesgamesbot";

    webhookSecretFile = mkOption {
      type = types.str;
    };
  };

  config = mkIf cfg.enable {
    systemd.timers."nytimesgamesbot" = {
      wantedBy = ["timers.target"];
      timerConfig = {
        OnCalendar = "*-*-* 00:00:00";
        Persistent = true;
      };
    };

    systemd.services."nytimesgamesbot" = {
      script = ''
        WEBHOOK_URL=$(cat $WEBHOOK_URL_FILE)

        generate_webhook() {
          local retries=5

          local count=0
          puzzleNum=""

          until [ ! -z "''${puzzleNum}" ]; do
            puzzleNum=$(chromium --no-sandbox --headless --virtual-time-budget=10000 --dump-dom "https://www.nytimes.com/games/$puzzle" | htmlq "$filter" --text)
            exit=$?
            wait=$((2 ** $count))
            count=$(($count + 1))
            if [ $count -lt $retries ]; then
              echo "Retry $count/$retries exited $exit, retrying in $wait seconds..."
              sleep $wait
            else
              echo "Retry $count/$retries exited $exit, no more retries left."
            fi
          done

          image="https://www.nytimes.com/games-assets/v2/assets/icons/$puzzle.svg"
          url="https://www.nytimes.com/games/$puzzle"

          payload=$(cat <<EOF
          {
            "username": "Puzzles",
            "icon_url": "$image",
            "text": "''${puzzle^} $puzzleNum: [Link]($url)"
          }
        EOF
        )

          curl -i -X POST --data-urlencode "payload=$payload" "$WEBHOOK_URL"
        }

        connections() {
          puzzle="connections"
          filter=".pz-moment__info-puzzleNumber"
          generate_webhook
        }

        strands() {
          puzzle="strands"
          filter=".pz-game-date"
          generate_webhook
        }

        wordle() {
          puzzle="wordle"
          filter="div:nth-of-type(3) > p:nth-of-type(1)"
          generate_webhook
        }

        connections
        strands
        wordle
      '';

      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      path = with pkgs; [
        bash
        curl
        chromium
        htmlq
      ];
      environment = {
        WEBHOOK_URL_FILE = cfg.webhookSecretFile;
      };
    };
  };
}
