{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.modules.jellyfin;
in {
  options.modules.jellyfin = with lib;
  with types; {
    enable = mkEnableOption "jellyfin";

    useIntelHardware = mkOption {
      type = bool;
      default = false;
    };
  };

  config = mkIf cfg.enable {
    services.jellyfin = {
      enable = true;
      openFirewall = true;
    };

    services.nginx.virtualHosts = {
      "jellyfin.${config.networking.hostName}.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;
        acmeRoot = null;

        locations."/" = {
          proxyPass = "http://127.0.0.1:8096";
          proxyWebsockets = true;
        };
      };
    };

    nixpkgs.config.packageOverrides = pkgs:
      if cfg.useIntelHardware
      then {
        vaapiIntel = pkgs.vaapiIntel.override {enableHybridCodec = true;};
      }
      else {};

    hardware.opengl = mkIf cfg.useIntelHardware {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        vaapiIntel
        vaapiVdpau
        libvdpau-va-gl
        intel-compute-runtime # OpenCL filter support (hardware tonemapping and subtitle burn-in)
      ];
    };
  };
}
