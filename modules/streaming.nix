{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  nixos_old = import (fetchTarball https://github.com/nixos/nixpkgs/tarball/nixos-19.03) {};
  cfg = config.modules.music-streaming;
in {
  options.modules.music-streaming = {
    enable = mkEnableOption "music streaming services";

    spotify_username = mkOption {
      type = types.str;
    };

    spotify_password = mkOption {
      type = types.str;
    };

    spotify_client_id = mkOption {
      type = types.str;
    };

    spotify_client_secret = mkOption {
      type = types.str;
    };

    icecast_admin_password = mkOption {
      type = types.str;
    };

    use_ssl = mkOption {
      type = types.bool;
    };

    domain_name = mkOption {
      type = types.str;
    };

    mopidy_log_verbosity = mkOption {
      type = types.str;
      default = "2";
    };

    iris_country = mkOption {
      type = types.str;
      default = "us";
    };

    iris_locale = mkOption {
      type = types.str;
      default = "en_US";
    };
  };

  config = mkIf cfg.enable {
    # incredible hack that gets a working libshout for mopidy to be able to `shout2send` to icecast
    nixpkgs.config.packageOverrides = super: let self = super.pkgs; in {libshout = nixos_old.pkgs.libshout;};
    nixpkgs.config.allowUnfree = true;

    # "${pkgs.icecast}/share/icecast/web/silence.mp3" = writeText "myfile";

    services.mopidy = let
      protocol =
        if cfg.use_ssl
        then "https"
        else "http";
    in {
      enable = true;
      configuration = ''
        [http]
        enabled = true
        hostname = 127.0.0.1
        port = 6680
        zeroconf =
        allowed_origins = ${protocol}://icecast.${cfg.domain_name}
        csrf_protection = false
        default_app = mopidy

        [spotify]
        username = ${cfg.spotify_username}
        password = ${cfg.spotify_password}
        client_id = ${cfg.spotify_client_id}
        client_secret = ${cfg.spotify_client_secret}
        bitrate=320

        [iris]
        country = ${cfg.iris_country}
        locale = ${cfg.iris_locale}

        [audio]
        output = lamemp3enc target=quality quality=0 ! shout2send async=false mount=mopidy ip=127.0.0.1 port=8000 username=source password=hackme

        [logging]
        verbosity = ${cfg.mopidy_log_verbosity}
      '';

      extensionPackages = [
        pkgs.mopidy-spotify
        pkgs.mopidy-iris
      ];
    };

    # See: https://docs.mopidy.com/en/latest/audio/#streaming-through-icecast
    services.icecast = {
      enable = true;
      hostname = "${config.networking.hostName}.${config.networking.domain}";
      listen.address = "0.0.0.0";
      admin.password = cfg.icecast_admin_password;
      extraConf = ''
        <mount>
          <mount-name>/mopidy</mount-name>
          <username>source</username>
          <password>hackme</password>
        </mount>
      '';
      /*
      <fallback-mount>/silence.mp3</fallback-mount>
      <fallback-override>1</fallback-override>
      <logging>
        <loglevel>4</loglevel>
      </logging>
      */
    };

    services.nginx = {
      package = pkgs.nginx.override {modules = [pkgs.nginxModules.subsFilter];}; # add PAM module

      virtualHosts = {
        "music.${cfg.domain_name}" = {
          forceSSL = cfg.use_ssl;
          enableACME = cfg.use_ssl;

          locations."/" = {
            proxyPass = "http://127.0.0.1:6680";
            proxyWebsockets = true;
            extraConfig = ''
              location = / {
                return 301 /iris/;
              }
            '';
          };
        };

        "icecast.${cfg.domain_name}" = {
          forceSSL = cfg.use_ssl;
          enableACME = cfg.use_ssl;

          extraConfig = ''
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Host $host;
            proxy_set_header X-Forwarded-Server $host;
            proxy_set_header X-Real-IP $remote_addr;
          '';

          locations."/" = {
            proxyPass = "http://127.0.0.1:8000";
            proxyWebsockets = true;
            extraConfig = ''
              subs_filter_types application/xspf+xml audio/x-mpegurl audio/x-vclt text/css text/html text/xml;
              subs_filter ':8000/' '/' gi;
              subs_filter '@localhost' '@icecast.kanyid.org' gi;
              subs_filter 'localhost' $host gi;
              subs_filter 'Mount Point ' $host gi;
              subs_filter '<h1 id="header">Icecast2 Status</h1><div id="menu"><ul><li><a href="admin/">Administration</a></li><li><a href="status.xsl">Server Status</a></li><li><a href="server_version.xsl">Version</a></li></ul></div>' '<!--<div id="menu"><ul><li><a href="admin/">Administration</a></li><li><a href="status.xsl">Server Status</a></li><li><a href="server_version.xsl">Version</a></li></ul></div>-->' gi;

              location /admin/ {
                deny all;
              }

              location /server_version.xsl {
                deny all;
              }
            '';
          };
        };
      };
    };

    # networking.firewall.allowedTCPPorts = [ 8000 ];
  };
}
