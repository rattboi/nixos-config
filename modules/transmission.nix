{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.modules.transmission;
in {
  options.modules.transmission = with types; {
    enable = mkEnableOption "transmission";

    download_dir = mkOption {
      type = uniq str;
    };

    blocklist_url = mkOption {
      type = uniq str;
    };

    credentials_file = mkOption {
      type = uniq str;
    };
  };

  config = mkIf cfg.enable {
    systemd.services.transmission = {...}: {
      options = {
        requires = lib.mkOption {
          apply = defaults: defaults ++ ["mnt-storage.mount"];
        };
        after = lib.mkOption {
          apply = defaults: defaults ++ ["mnt-storage.mount"];
        };
        unitConfig = lib.mkOption {
          apply = defaults: defaults // {RequiresMountsFor = cfg.download_dir;};
        };
      };
    };

    services.transmission = {
      enable = true;

      settings = {
        download-dir = "${cfg.download_dir}";
        incomplete-dir = "${cfg.download_dir}/.incomplete/";
        incomplete-dir-enabled = true;

        blocklist-url = "${cfg.blocklist_url}";
        blocklist-enabled = true;

        rpc-bind-address = "127.0.0.1";
        rpc-whitelist = "127.0.0.1,192.168.*.*";
        rpc-host-whitelist = "127.0.0.1,192.168.*.*";

        rpc-authentication-required = true;
        umask = 2;
      };

      credentialsFile = cfg.credentials_file;

      downloadDirPermissions = "775";
    };

    networking.firewall.allowedTCPPorts = [9091 51413];
    networking.firewall.allowedUDPPorts = [51413];

    services.nginx.virtualHosts = {
      "transmission.${config.networking.hostName}.${config.networking.domain}" = {
        enableACME = true;
        forceSSL = true;
        acmeRoot = null;
        locations."/" = {
          proxyPass = "http://127.0.0.1:${toString config.services.transmission.settings.rpc-port}";
          proxyWebsockets = true;
        };
      };
    };
  };
}
