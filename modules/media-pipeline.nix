{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.media-pipeline;
  sonarrPort = 8989;
  radarrPort = 7878;
in {
  options.services.media-pipeline = with lib;
  with types; {
    enable = mkEnableOption "media-pipeline";

    unpackerrUser = mkOption {
      type = str;
    };

    unpackerrGroup = mkOption {
      type = str;
    };

    unpackerrEnvironmentFile = mkOption {
      type = str;
    };
  };

  config = mkIf cfg.enable {
    nixpkgs.config.permittedInsecurePackages = [
      "aspnetcore-runtime-6.0.36"
      "aspnetcore-runtime-wrapped-6.0.36"
      "dotnet-sdk-6.0.428"
      "dotnet-sdk-wrapped-6.0.428"
    ];

    services = {
      # sonarr
      sonarr = {
        enable = true;
        openFirewall = true;
      };

      nginx.virtualHosts."sonarr.${config.networking.hostName}.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;
        acmeRoot = null;

        locations."/" = {
          proxyPass = "http://127.0.0.1:${toString sonarrPort}";
          proxyWebsockets = true;
        };
      };

      # radarr
      radarr = {
        enable = true;
        openFirewall = true;
      };

      nginx.virtualHosts."radarr.${config.networking.hostName}.${config.networking.domain}" = {
        forceSSL = true;
        enableACME = true;
        acmeRoot = null;

        locations."/" = {
          proxyPass = "http://127.0.0.1:${toString radarrPort}";
          proxyWebsockets = true;
        };
      };

      # unpackerr
      unpackerr = {
        enable = true;
        debug = true;

        user = cfg.unpackerrUser;
        group = cfg.unpackerrGroup;
        environmentFile = cfg.unpackerrEnvironmentFile;

        sonarr = {
          url = "http://localhost:${toString sonarrPort}/";
          paths = "/mnt/storage/transmission/shows-unsorted";
        };

        radarr = {
          url = "http://localhost:${toString radarrPort}/";
          paths = "/mnt/storage/transmission/movies-unsorted";
        };
      };

      # jackett
      jackett = {
        enable = true;
        openFirewall = true;
      };
    };
  };
}
