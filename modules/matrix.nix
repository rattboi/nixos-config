{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  # unstable = fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz";
  # unstablePkgs = import unstable { };
  cfg = config.modules.matrix;
in {
  #  imports = [
  #    "${unstable}/nixos/modules/services/misc/matrix-appservice-irc.nix"
  #  ];

  options.modules.matrix = {
    enable = mkEnableOption "matrix services";

    use_ssl = mkOption {
      type = types.bool;
    };

    domain_name = mkOption {
      type = types.str;
    };
  };

  config = mkIf cfg.enable {
    #    nixpkgs.overlays = [
    #      (self: super:
    #        {
    #          # apparently matrix-appservice-irc hasn't landed in stable yet
    #          matrix-appservice-irc = unstablePkgs.matrix-appservice-irc;
    #        }
    #      )
    #    ];

    networking.firewall = {
      allowedTCPPorts = [
        80 # http
        443 # https
      ];
    };

    services.nginx = {
      virtualHosts = {
        "matrix.${cfg.domain_name}" = {
          forceSSL = cfg.use_ssl;
          enableACME = cfg.use_ssl;

          locations."/" = {
            proxyPass = "http://localhost:8008";
          };

          locations."= /.well-known/matrix/server".extraConfig = let
            # use 443 instead of the default 8448 port to unite
            # the client-server and server-server port for simplicity
            server = {"m.server" = "matrix.${cfg.domain_name}:443";};
          in ''
            add_header Content-Type application/json;
            return 200 '${builtins.toJSON server}';
          '';
        };

        "element.${cfg.domain_name}" = {
          forceSSL = cfg.use_ssl;
          enableACME = cfg.use_ssl;

          locations."/" = {
            root = pkgs.element-web.override {
              conf = {
                default_server_config = {
                  "m.homeserver" = {
                    "base_url" = "https://matrix.${cfg.domain_name}";
                    "server_name" = cfg.domain_name;
                  };
                  "m.identity_server" = {
                    "base_url" = "https://vector.im";
                  };
                };
              };
            };
          };
        };

        "${cfg.domain_name}" = {
          locations."= /.well-known/matrix/server".extraConfig = let
            # use 443 instead of the default 8448 port to unite
            # the client-server and server-server port for simplicity
            server = {"m.server" = "matrix.${cfg.domain_name}:443";};
          in ''
            add_header Content-Type application/json;
            return 200 '${builtins.toJSON server}';
          '';
        };
      };

      recommendedGzipSettings = true;
      recommendedOptimisation = true;
      recommendedTlsSettings = true;
    };

    services.matrix-synapse = {
      enable = true;

      settings = {
        server_name = cfg.domain_name;
        enable_metrics = true;
        enable_registration = false;
        database_type = "psycopg2";
        database_args = {
          password = "synapse";
        };

        ## default http listener which nginx will passthrough to
        listeners = [
          {
            # client
            bind_addresses = [
              "127.0.0.1"
            ];
            port = 8008;
            resources = [
              {
                compress = true;
                names = ["client" "federation"];
              }
            ];
            tls = false;
            type = "http";
          }
        ];

        app_service_config_files = [
          # The registration file is automatically generated after starting the
          # appservice for the first time.
          # cp /var/lib/mautrix-telegram/telegram-registration.yaml \
          #   /var/lib/matrix-synapse/
          # chown matrix-synapse:matrix-synapse \
          #   /var/lib/matrix-synapse/telegram-registration.yaml
          "/var/lib/matrix-appservice-irc/registration.yml"
        ];
      };
    };

    services.postgresql = {
      enable = true;

      ## postgresql user and db name remains in the
      ## service.matrix-synapse.database_args setting which
      ## by default is matrix-synapse
      initialScript = pkgs.writeText "synapse-init.sql" ''
        CREATE ROLE "matrix-synapse" WITH LOGIN PASSWORD 'synapse';
        CREATE DATABASE "matrix-synapse" WITH OWNER "matrix-synapse"
          TEMPLATE template0
          LC_COLLATE = "C"
          LC_CTYPE = "C";
      '';
    };

    services.matrix-appservice-irc = {
      enable = true;
      registrationUrl = "http://localhost:8009";

      settings = {
        homeserver.url = "https://matrix.${cfg.domain_name}"; # Or localhost
        homeserver.domain = cfg.domain_name;
        ircService.servers."irc.cat.pdx.edu" = {
          name = "thecat";
          port = 6697;
          ssl = true;
          sslselfsign = true;

          botConfig = {
            nick = "rattbridge";
            username = "matrixbot";
            password = "9iHnG7eMYqA5SvSCscnV";
            joinChannelsIfNoUsers = true;
            enabled = true;
          };

          dynamicChannels = {
            enabled = true;
            aliasTemplate = "#irc_$CHANNEL";
            groupId = "+irc:localhost";
          };
          matrixClients = {
            userTemplate = "@irc_$NICK";
          };
          ircClients = {
            nickTemplate = "$LOCALPART[m]";
            allowNickChanges = true;
          };

          membershipLists = {
            enabled = true;
            global = {
              ircToMatrix = {
                initial = true;
                incremental = true;
              };
              matrixToIrc = {
                initial = true;
                incremental = true;
              };
            };
          };
        };
      };
    };

    environment = {
      systemPackages = with pkgs; [
        element-web
      ];
    };
  };
}
