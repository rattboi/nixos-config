{
  config,
  pkgs,
  lib,
  ...
}: let
  pushgateway_port = 9092;
in {
  # grafana configuration
  services.grafana = {
    enable = true;

    settings = {
      server = {
        domain = "grafana.${config.networking.hostName}.${config.networking.domain}";
        http_port = 2342;
        http_addr = "127.0.0.1";
      };
    };
  };

  services.prometheus = {
    enable = true;
    port = 9090;

    pushgateway = {
      enable = true;
      web.listen-address = "localhost:${toString pushgateway_port}";
    };

    exporters = {
      node = {
        enable = true;
        enabledCollectors = ["systemd"];
        port = 9100;
      };
    };

    scrapeConfigs = [
      {
        job_name = "rattnix";
        static_configs = [
          {
            targets = [
              "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" # node exporter
              "127.0.0.1:${toString pushgateway_port}" # push gateway
            ];
          }
        ];
      }
    ];
  };

  services.loki = let
    lokiConfig = pkgs.writeText "loki.conf" ''
      auth_enabled: false

      server:
        http_listen_port: 3100

      ingester:
        lifecycler:
          address: 0.0.0.0
          ring:
            kvstore:
              store: inmemory
            replication_factor: 1
          final_sleep: 0s
        chunk_idle_period: 1h       # Any chunk not receiving new logs in this time will be flushed
        max_chunk_age: 1h           # All chunks will be flushed when they hit this age, default is 1h
        chunk_target_size: 1048576  # Loki will attempt to build chunks up to 1.5MB, flushing first if chunk_idle_period or max_chunk_age is reached first
        chunk_retain_period: 30s    # Must be greater than index read cache TTL if using an index cache (Default index read cache TTL is 5m)
        max_transfer_retries: 0     # Chunk transfers disabled

      schema_config:
        configs:
          - from: 2020-10-24
            store: boltdb-shipper
            object_store: filesystem
            schema: v11
            index:
              prefix: index_
              period: 24h

      compactor:
        working_directory: /var/lib/loki/compactor
        shared_store: filesystem
        delete_request_cancel_period: 10m # don't wait 24h before processing the delete_request
        retention_enabled: true # actually do the delete
        retention_delete_delay: 2h # wait 2 hours before actually deleting stuff

      storage_config:
        boltdb_shipper:
          active_index_directory: /var/lib/loki/boltdb-shipper-active
          cache_location: /var/lib/loki/boltdb-shipper-cache
          cache_ttl: 24h         # Can be increased for faster performance over longer query periods, uses more disk space
          shared_store: filesystem
        filesystem:
          directory: /var/lib/loki/chunks

      limits_config:
        reject_old_samples: true
        reject_old_samples_max_age: 168h
        retention_period: 10d # Keep 10 days

      chunk_store_config:
        max_look_back_period: 0s

      table_manager:
        retention_deletes_enabled: false
        retention_period: 0s
    '';
  in {
    enable = true;
    configFile = "${lokiConfig}";
  };

  systemd.services.promtail = let
    promtailConfig = pkgs.writeText "promtail.conf" ''
      server:
        http_listen_port: 28183
        grpc_listen_port: 0

      positions:
        filename: /tmp/positions.yaml

      clients:
        - url: http://127.0.0.1:3100/loki/api/v1/push

      scrape_configs:
        - job_name: journal
          journal:
            max_age: 12h
            labels:
              job: systemd-journal
              host: rattnix
          relabel_configs:
            - source_labels: ['__journal__systemd_unit']
              target_label: 'unit'
    '';
  in {
    description = "Promtail service for Loki";
    wantedBy = ["multi-user.target"];

    serviceConfig = {
      ExecStart = ''
        ${lib.getExe' pkgs.grafana-loki "promtail"} --config.file ${promtailConfig}
      '';
    };
  };

  # nginx reverse proxy
  services.nginx.virtualHosts = {
    ${config.services.grafana.settings.server.domain} = {
      addSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.grafana.settings.server.http_port}";
        proxyWebsockets = true;
        extraConfig = ''
          proxy_set_header Host $host;
        '';
      };
    };

    "prometheus.${config.networking.hostName}.${config.networking.domain}" = {
      addSSL = true;
      enableACME = true;
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString config.services.prometheus.port}";
        proxyWebsockets = true;
      };
    };

    "pushgateway.${config.networking.hostName}.${config.networking.domain}" = {
      locations."/" = {
        proxyPass = "http://127.0.0.1:${toString pushgateway_port}";
        proxyWebsockets = true;
      };
    };
  };

  # open ports for web server
  networking.firewall.allowedTCPPorts = [80 443];
}
