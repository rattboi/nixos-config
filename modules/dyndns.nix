{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.dyndns;
in {
  options.services.dyndns = with lib;
  with types; {
    enable = mkEnableOption "dyndns";

    secretPath = mkOption {
      type = str;
    };

    startAt = mkOption {
      type = str;
      default = "*-*-* *:00:00"; # default to hourly
    };

    baseDomain = mkOption {
      type = uniq str;
    };

    subDomains = mkOption {
      type = listOf str;
    };

    ttl = mkOption {
      type = int;
      default = 600;
    };
  };

  config = mkIf cfg.enable {
    systemd.services."porkbun-dyndns" = {
      description = "Porkbun Dynamic DNS updater";
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      path = [
        pkgs.curl
        pkgs.jq
      ];
      startAt = cfg.startAt;
      script = ''
        # This script gets the external IP of your systems then connects to the
        # Porkbun API and updates your dns record with the IP

        TTL="${builtins.toString cfg.ttl}"

        test_domain() {
          DOMAIN="$1"
          SUBDOMAIN="$2"

          local result status

          result=$(curl -s -X POST -H "Content-Type: application/json" \
            -d "{\"secretapikey\": \"$PORKBUN_SECRET_API_KEY\",
                 \"apikey\": \"$PORKBUN_API_KEY\"}" \
            https://api.porkbun.com/api/json/v3/dns/retrieveByNameType/$DOMAIN/A/$SUBDOMAIN 2>/dev/null)

          status=$(echo $result | jq -r .status)

          if [[ "$status" != "SUCCESS" ]]; then
            echo "Couldn't fetch from api.porkbun.com API"
            exit 1
          else
            echo $(echo $result | jq '.records')
          fi
        }

        update_domain() {
          DOMAIN="$1"
          SUBDOMAIN="$2"
          EXT_IP="$3"

          local domain_record=$(test_domain "$DOMAIN" "$SUBDOMAIN")
          local domain_record_length=$(echo "$domain_record" | jq '. | length')

          if [[ "$domain_record_length" == "0" ]]; then
            echo "https://api.porkbun.com/api/json/v3/dns/create/$DOMAIN"
            curl -D- -X POST -H "Content-Type: application/json" \
              -d "{\"secretapikey\": \"$PORKBUN_SECRET_API_KEY\",
                   \"apikey\": \"$PORKBUN_API_KEY\",
                   \"name\": \"$SUBDOMAIN\",
                   \"type\": \"A\",
                   \"content\": \"$EXT_IP\",
                   \"ttl\": \"$TTL\"}" \
              https://api.porkbun.com/api/json/v3/dns/create/$DOMAIN
          else
            local existing_ttl=$(echo $domain_record | jq -r '.[].ttl')
            local existing_ip=$(echo $domain_record | jq -r '.[].content')

            if [[ "$existing_ttl" != "$TTL" || "$existing_ip" != "$EXT_IP" ]]; then
              curl -D- -X POST -H "Content-Type: application/json" \
                -d "{\"secretapikey\": \"$PORKBUN_SECRET_API_KEY\",
                     \"apikey\": \"$PORKBUN_API_KEY\",
                     \"content\": \"$EXT_IP\",
                     \"ttl\": \"$TTL\"}" \
                https://api.porkbun.com/api/json/v3/dns/editByNameType/$DOMAIN/A/$SUBDOMAIN
            fi
          fi
        }

        # DNS Registrar secrets
        source ${cfg.secretPath}

        # Main Domain
        BASE_DOMAIN="${cfg.baseDomain}"

        # Subdomain to update DNS
        SUBDOMAINS="${builtins.toString cfg.subDomains}"

        # Get external IP address
        EXT_IP=$(curl -s ifconfig.me)

        # Update the A Records of the subdomains
        for SUBDOMAIN in $SUBDOMAINS; do
          SUBDOMAIN_STANDALONE=''${SUBDOMAIN%".$BASE_DOMAIN"}
          update_domain "$BASE_DOMAIN" "$SUBDOMAIN_STANDALONE" "$EXT_IP"
        done
      '';
    };
  };
}
