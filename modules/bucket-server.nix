{
  config,
  pkgs,
  lib,
  ...
}:
with lib; let
  cfg = config.services.bucket-server;
in {
  options.services.bucket-server = with lib;
  with types; {
    enable = mkEnableOption "bucket-server";

    # systemd timer settings
    startAt = mkOption {
      type = str;
      default = "*-*-* *:00:00"; # every hour
    };

    # rclone settings
    syncDirs = mkOption {
      type = attrs;
    };

    globalFlags = mkOption {
      type = str;
      default = "";
    };

    bucketName = mkOption {
      type = str;
    };

    # s3 listing settings
    bucketUrl = mkOption {
      type = str;
    };

    indexTitle = mkOption {
      type = str;
      default = "S3 Bucket Listing Generator";
    };

    s3blIgnorePath = mkOption {
      type = str;
      default = "false";
    };

    excludeFile = mkOption {
      type = str;
      default = "index.html";
    };

    autoTitle = mkOption {
      type = str;
      default = "true";
    };

    # aws settings (for rclone)
    awsAccessKeyIdFile = mkOption {
      type = str;
    };

    awsSecretAccessKeyFile = mkOption {
      type = str;
    };

    awsDefaultRegion = mkOption {
      type = str;
      default = "us-east-1";
    };
  };

  config = mkIf cfg.enable {
    systemd.services.bucket-server = let
      # https://github.com/rufuspollock/s3-bucket-listing
      indexFile = builtins.toFile "index.html" ''
        <!DOCTYPE html>
        <html>
        <head>
          <title>${cfg.indexTitle}</title>
        </head>
        <body>
          <div id="navigation"></div>
          <div id="listing"></div>

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script type="text/javascript">
          var S3BL_IGNORE_PATH = ${cfg.s3blIgnorePath};
          var BUCKET_URL = '${cfg.bucketUrl}';
          var EXCLUDE_FILE = '${cfg.excludeFile}';
          var AUTO_TITLE = ${cfg.autoTitle};
          var BUCKET_WEBSITE_URL = 'http://${cfg.bucketName}'
        </script>
        <script type="text/javascript" src="https://rufuspollock.github.io/s3-bucket-listing/list.js"></script>
        </body>
        </html>
      '';
      rcloneConfig = builtins.toFile "rclone.conf" ''
        [remote]
        type = s3
        provider = AWS
        env_auth = true
        region = ${cfg.awsDefaultRegion}
        location_constraint = ${cfg.awsDefaultRegion}
        acl = public-read
        server_side_encryption = AES256
        storage_class = STANDARD
      '';
    in {
      description = "Bucket Server";
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      environment = {
        AWS_DEFAULT_REGION = cfg.awsDefaultRegion;
      };
      path = [
        pkgs.rclone
      ];
      startAt = cfg.startAt;
      script = ''
        set +e

        export AWS_ACCESS_KEY_ID=$(cat ${cfg.awsAccessKeyIdFile});
        export AWS_SECRET_ACCESS_KEY=$(cat ${cfg.awsSecretAccessKeyFile});
        ${lib.getExe pkgs.rclone} --config="${rcloneConfig}" ${cfg.globalFlags} copyto ${indexFile} remote:${cfg.bucketName}/index.html
        ${
          concatStringsSep "\n"
          (forEach (attrNames cfg.syncDirs) (syncDir: ''${lib.getExe pkgs.rclone} --config="${rcloneConfig}" ${cfg.globalFlags} --track-renames sync ${syncDir} remote:${cfg.bucketName}/${cfg.syncDirs.${syncDir}}''))
        }
      '';
    };
  };
}
