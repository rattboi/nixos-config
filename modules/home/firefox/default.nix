{
  config,
  pkgs,
  ...
}: {
  programs.firefox = {
    enable = true;
    package = pkgs.firefox-wayland.override {
      nativeMessagingHosts = [
        pkgs.tridactyl-native
        pkgs.firefoxpwa
      ];
    };
    profiles = {
      default = {
        extensions = with pkgs.nur.repos.rycee.firefox-addons; [
          bitwarden
          darkreader
          privacy-badger
          tree-style-tab
          tridactyl
          ublock-origin
          web-scrobbler
          pwas-for-firefox
        ];

        userChrome = ''
          #TabsToolbar {
              visibility: collapse !important;
          }

          /* As little space before the tab name as possible.
             The fold/unfold icon is not affected. */
          .tab:not(.pinned) {
            padding-left: 0px !important; /* !important is required when there are enough tabs to cause a scrollbar */
          }

          /* Less visible tab dividers.
             A black border with a very low alpha slightly darkens any color. */
          .tab {
            border: solid 1px #00000012;
          }

          .tab {
            height: 20px;
          }

          #sidebar-box[sidebarcommand="treestyletab_piro_sakura_ne_jp-sidebar-action"] #sidebar-header {
            display: none;
          }

          #sidebar {
            min-width: 100px !important;
          }

          #navigator-toolbox:focus-within > #nav-bar,
          #navigator-toolbox:hover > #nav-bar
          {
            margin-top: 0;
            margin-bottom: var(--navbar-margin);
            z-index: 100;
            opacity: 1;
          }
        '';

        settings = {
          # needed for above userChrome to work
          "toolkit.legacyUserProfileCustomizations.stylesheets" = true;

          "signon.rememberSignons" = false;
          # "widget.use-xdg-desktop-portal.file-picker" = 1;
          "browser.aboutConfig.showWarning" = false;
          "browser.compactmode.show" = true;
          "browser.cache.disk.enable" = false; # Be kind to hard drive
          "browser.tabs.loadDivertedInBackground" = true; # links explicitly opened in new tab (like middle-click) are not given focus
        };

        search = {
          force = true;
          default = "Google";
          engines = {
            "Nix Packages" = {
              urls = [
                {
                  template = "https://search.nixos.org/packages";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = ["@np"];
            };
            "NixOS Options" = {
              urls = [
                {
                  template = "https://search.nixos.org/options";
                  params = [
                    {
                      name = "type";
                      value = "packages";
                    }
                    {
                      name = "query";
                      value = "{searchTerms}";
                    }
                  ];
                }
              ];
              icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
              definedAliases = ["@no"];
            };

            "NixOS Wiki" = {
              urls = [
                {
                  template = "https://nixos.wiki/index.php?search={searchTerms}";
                }
              ];
              iconUpdateURL = "https://nixos.wiki/favicon.png";
              updateInterval = 24 * 60 * 60 * 1000; # every day
              definedAliases = ["@nw"];
            };
          };
        };
      };
    };
  };

  xdg.configFile."tridactyl/tridactylrc".source = ./tridactylrc;
}
