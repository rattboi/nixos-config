{pkgs, ...}: {
  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    delta.enable = true;
    userName = "Bradon Kanyid (rattboi)";
    userEmail = "rattboi@gmail.com";
  };
}
