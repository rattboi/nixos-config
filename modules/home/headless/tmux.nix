{pkgs, ...}: {
  home = {
    packages = with pkgs; [
      tmux
    ];
  };

  programs = {
    tmux = {
      enable = true;
      shortcut = "Space";
      keyMode = "vi";
      customPaneNavigationAndResize = true;
      reverseSplit = true;
      terminal = "tmux-256color";

      mouse = true;
      baseIndex = 1;
      extraConfig = ''
        # Set status bar
        set -g status-bg black
        set -g status-fg white
        set -g status-left '#[fg=green]#H '
        set -g status-right '#[fg=yellow]#(uptime | cut -d "," -f 3-) #(sensors | grep Core | cut -d" " -f 9 | tr "\n" " ")'
        set -g status-right-length 50
      '';
    };
  };
}
