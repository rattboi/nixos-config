{
  inputs,
  pkgs,
  ...
}: {
  programs.bat = {
    enable = true;
    extraPackages = with pkgs.bat-extras; [
      #batdiff - uncomment in a few days
      batman
    ];
  };

  programs.zsh.shellAliases = {
    cat = "bat";
    man = "batman";
    #diff = "batdiff";
  };

  programs.fish.shellAliases = {
    cat = "bat";
    man = "batman";
    #diff = "batdiff";
  };
}
