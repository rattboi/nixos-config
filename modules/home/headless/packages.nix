{pkgs, ...}: {
  home.packages = with pkgs; [
    kind
    k9s
    tree
    glances
    libnotify
    lazygit
  ];
}
