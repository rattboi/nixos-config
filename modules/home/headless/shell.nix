{
  inputs,
  pkgs,
  lib,
  config,
  ...
}: let
  persistHomeDir = "/persist/home/${config.home.username}";
in {
  programs = {
    zsh =
      {
        enable = true;
        # skip the symlink and go right to the source
        oh-my-zsh = {
          enable = true;
          plugins = [
            "git"
            "vi-mode"
            "docker"
            "sudo"
          ];
          theme = "flazz";
        };
        initExtra = ''
          source ${pkgs.nix-index}/etc/profile.d/command-not-found.sh
        '';
      }
      // lib.optionalAttrs (builtins.hasAttr "persistence" config.home) {
        history.path = "${persistHomeDir}/.zsh_history";
      };

    # setup command-not-found with nix-index
    nix-index.enable = true;
    command-not-found.enable = false;

    # for nix-shell
    bash =
      {
        enable = true;
      }
      // lib.optionalAttrs (builtins.hasAttr "persistence" config.home) {
        historyFile = "${persistHomeDir}./bash_history";
      };
  };
}
