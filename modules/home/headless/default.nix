{...}: {
  imports = [
    ./bat.nix
    ./direnv.nix
    ./git.nix
    ./nvim
    ./packages.nix
    ./readline.nix
    ./shell.nix
    ./tmux.nix
  ];
}
