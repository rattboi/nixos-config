{
  config,
  lib,
  ...
}: {
  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };

  home = lib.optionalAttrs (builtins.hasAttr "persistence" config.home) {
    persistence."/persist/home/${config.home.username}" = {
      directories = [
        ".local/share/direnv"
      ];
    };
  };
}
