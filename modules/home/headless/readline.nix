{...}: {
  programs.readline = {
    enable = true;
    variables = {
      completion-ignore-case = true;
      expand-tilde = true;
      convert-meta = false;
      input-meta = true;
      output-meta = true;
      show-all-if-ambiguous = true;
      visible-stats = true;

      editing-mode = "vi";
      show-mode-in-prompt = true;
    };
  };
}
