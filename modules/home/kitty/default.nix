{pkgs, ...}: {
  home = {
    packages = with pkgs; [
      kitty
    ];
  };

  programs = {
    kitty = {
      enable = true;
      font = {
        name = "DejaVu Sans";
        size = 15.0;
      };
      shellIntegration = {
        enableBashIntegration = true;
        enableZshIntegration = true;
      };
    };
  };
}
