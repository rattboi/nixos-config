{
  pkgs,
  config,
  lib,
  ...
}:
with lib; let
  cfg = config.services."1001bot";
in {
  options.services."1001bot" = with lib;
  with types; {
    enable = mkEnableOption "1001bot";

    username = mkOption {
      type = types.str;
    };

    albumgeneratorUrl = mkOption {
      type = types.str;
    };

    webhookSecretFile = mkOption {
      type = types.str;
    };
  };

  config = mkIf cfg.enable {
    systemd.timers."1001bot" = {
      wantedBy = ["timers.target"];
      timerConfig = {
        OnCalendar = "Mon..Fri *-*-* 00:00:00";
        Persistent = true;
      };
    };

    systemd.services."1001bot" = {
      script = ''
        #!/usr/bin/env bash

        PAYLOAD_FILE=$(mktemp)
        WEBHOOK_URL=$(cat $WEBHOOK_URL_FILE)

        curl "$ALBUMGENERATOR_URL" > "$PAYLOAD_FILE"

        webhook_url=$(cat $WEBHOOK_URL_FILE)
        username="$BOT_USERNAME"

        albumNumber=$( cat  $PAYLOAD_FILE | jq '.numberOfGeneratedAlbums')
        albumNumber=$(($albumNumber + 1))
        currentAlbum=$(cat  $PAYLOAD_FILE | jq '.currentAlbum')
        album=$(       echo $currentAlbum | jq -r '.name')
        albumYear=$(   echo $currentAlbum | jq -r '.releaseDate')
        artist=$(      echo $currentAlbum | jq -r '.artist')
        image=$(       echo $currentAlbum | jq -r '.images[0].url')
        spotifyId=$(   echo $currentAlbum | jq -r '.spotifyId')
        wiki=$(        echo $currentAlbum | jq -r '.wikipediaUrl')

        payload=$(cat <<EOF
        {
          "username": "$username",
          "attachments": [
            {
              "thumb_url": "$image",
              "fields": [
                {
                  "title": "Artist",
                  "value": "$artist",
                  "short": true
                },
                {
                  "title": "Album",
                  "value": "$album ($albumYear)",
                  "short": true
                },
                {
                  "title": "Wikipedia",
                  "value": "<$wiki|Link>",
                  "short": true
                },
                {
                  "title": "Spotify",
                  "value": "<spotify:album:$spotifyId|App> - <https://open.spotify.com/album/$spotifyId|Web>",

                  "short": true
                }
              ],
              "title": "1001 Albums #$albumNumber"
            }
          ]
        }
        EOF
        )

        echo $payload

        rm $PAYLOAD_FILE

        curl -i -X POST --data-urlencode "payload=$payload" $webhook_url
      '';
      serviceConfig = {
        Type = "oneshot";
        User = "root";
      };
      path = [
        pkgs.bash
        pkgs.jq
        pkgs.curl
      ];
      environment = {
        BOT_USERNAME = cfg.username;
        ALBUMGENERATOR_URL = cfg.albumgeneratorUrl;
        WEBHOOK_URL_FILE = cfg.webhookSecretFile;
      };
    };
  };
}
