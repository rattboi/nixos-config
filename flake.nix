{
  description = "Nixos config flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    nixpkgs-stable.url = "github:nixos/nixpkgs/nixos-24.11";

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-hardware.url = "github:NixOS/nixos-hardware/master";

    impermanence.url = "github:nix-community/impermanence";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager-stable = {
      url = "github:nix-community/home-manager/release-24.11";
      inputs.nixpkgs.follows = "nixpkgs-stable";
    };

    nur.url = "github:nix-community/NUR";

    hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1";
    hyprland-plugins = {
      url = "github:hyprwm/hyprland-plugins";
      inputs.hyprland.follows = "hyprland";
    };

    nix-index-database.url = "github:Mic92/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";

    sops-nix.url = "github:Mic92/sops-nix";
    sops-nix.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    nixpkgs-stable,
    disko,
    nixos-hardware,
    impermanence,
    home-manager,
    home-manager-stable,
    nur,
    hyprland,
    nix-index-database,
    sops-nix,
    ...
  }: let
    system = "x86_64-linux";

    pkgs =
      import nixpkgs {
        inherit system;
        config.allowUnfree = true;
        overlays =
          [nur.overlays.default]
          ++ import ./hosts/ratttop/overlays {inherit pkgs;};
      }
      // {outPath = nixpkgs.outPath;};
    pkgs-stable =
      import nixpkgs-stable {
        inherit system;
        config.allowUnfree = true;
      }
      // {outPath = nixpkgs-stable.outPath;};

    pkgs-unstable = pkgs;
  in {
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.alejandra;

    nixosConfigurations = {
      ratttop = nixpkgs.lib.nixosSystem rec {
        inherit system pkgs;
        specialArgs = {inherit inputs pkgs-stable;};
        modules = [
          # disk things
          disko.nixosModules.default
          (import ./hosts/ratttop/disko.nix {device = "/dev/nvme0n1";})

          # other hardware things
          nixos-hardware.nixosModules.framework-16-7040-amd

          # system config things
          ./hosts/ratttop/configuration.nix
          nur.modules.nixos.default

          impermanence.nixosModules.impermanence

          hyprland.nixosModules.default
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.extraSpecialArgs = specialArgs;
            home-manager.users.rattboi.imports = [
              impermanence.nixosModules.home-manager.impermanence
              nix-index-database.hmModules.nix-index
              ./hosts/ratttop/rattboi-home.nix
            ];
          }

          sops-nix.nixosModules.sops
          {
            sops = {
              defaultSopsFile = ./secrets/secrets.yaml;
              age.sshKeyPaths = ["/persist/home/rattboi/.ssh/id_ed25519"];
              secrets = {
                "samba/mnt-share" = {};
              };
            };
          }
        ];
      };

      rattnix = nixpkgs-stable.lib.nixosSystem rec {
        inherit system;
        specialArgs = {inherit inputs pkgs-unstable;};
        modules = [
          (nixpkgs-stable + "/nixos/modules/installer/scan/not-detected.nix")
          ./hosts/rattnix/configuration.nix

          home-manager-stable.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.backupFileExtension = "backup";
            home-manager.users.rattboi.imports = [
              nix-index-database.hmModules.nix-index
              ./hosts/rattnix/rattboi-home.nix
            ];
          }

          sops-nix.nixosModules.sops
          {
            sops = {
              defaultSopsFile = ./secrets/secrets.yaml;
              age.sshKeyPaths = ["/home/rattboi/.ssh/id_ed25519"];
              secrets = {
                "bucket_server/aws_access_key_id" = {};
                "bucket_server/aws_secret_access_key" = {};
                "transmission/credentials.json" = {
                  owner = "transmission";
                };
                "1001bot/webhook_url" = {};
                "nytimesgamesbot/webhook_url" = {};
                "porkbun/credentials_file" = {};
                "mailpass" = {};
                "unpackerr/environmentFile" = {
                  owner = "transmission";
                };
                "storyteller/environmentFile" = {};
              };
            };
          }
        ];
      };

      rattbox360 = nixpkgs.lib.nixosSystem rec {
        inherit system pkgs;
        modules = [
          (nixpkgs + "/nixos/modules/installer/scan/not-detected.nix")
          ./hosts/rattbox360/configuration.nix

          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.backupFileExtension = "backup";
            home-manager.users.htpc.imports = [
              nix-index-database.hmModules.nix-index
              ./hosts/rattbox360/home.nix
            ];
          }
        ];
      };
    };
  };
}
